import * as SYSTEM_CONST from "../globalConstants";
import axios from "axios";
import firebase from "firebase/app"
import "firebase/database"

export const DUMP_TO_FIREBASE_PUBLIC = function (url, method, error) {
  let database = firebase.database();
  let log = {
    url: url,
    method: method,
    status: error.status
  };
  if(error.statusText) {
    log.statusText = error.statusText
  }
  if(error.data) {
    log.data = error.data
  }
  database.ref('web-application/public/').push({
    time : new Date().getTime(),
    log: log
  }).catch()
};


export const SEARCH_JOBS_PUBLIC = function searchJobs(params) {
  let data = params.top
    ? {
        top: true
      }
    : {
        q: params.q
      };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PUBLIC_SEARCH,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: data
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_SEARCH, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_SEARCH, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};
export const GET_JOB_DETAILS = function(id,trending=false) {
  let data = {
    job_id: id
  };
  trending&&(data.source="search");
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const CONTACT_US = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.CONTACT_US,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      data: JSON.stringify(data)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.CONTACT_US, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.CONTACT_US, SYSTEM_CONST.METHOD.POST, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const SUBSCRIPTION_PLANS_PUBLIC = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.SUBSCRIPTION_PLANS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.SUBSCRIPTION_PLANS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.SUBSCRIPTION_PLANS, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_API = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: "https://api.ipify.org?format=json"
    })
      .then(function(response) {
        // console.log(response);
      })
      .catch(function(err) {
        // console.log(err);
      });
  });
};

export const GET_RECENT_SEARCHES = function() {
  let params = {
    search_capsule: true
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const SEARCH_RESUME_PUBLIC = function searchJobs(key) {
  let data = {
    q: key
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PUBLIC_RESUME_SEARCH,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: data
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_RESUME_SEARCH, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_RESUME_SEARCH, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_FEATURED_COMPANY_IMAGES = function() {
  let url = SYSTEM_CONST.getBucketUrl();
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: url
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(url, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(url, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_COMPANY_DETAILS = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PUBLIC_COMPANY_PROFILE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_COMPANY_PROFILE, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_COMPANY_PROFILE, SYSTEM_CONST.METHOD.GET, err.response);
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_NEWS_PUBLIC = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.NEWS_PUBLIC,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      params: data
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        resolve(response.data);
      } else {
        DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_COMPANY_PROFILE, SYSTEM_CONST.METHOD.GET, response);
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      DUMP_TO_FIREBASE_PUBLIC(SYSTEM_CONST.BASE_URL.PUBLIC_COMPANY_PROFILE, SYSTEM_CONST.METHOD.GET, err.response);
      err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
      reject(err);
    });
  });
};