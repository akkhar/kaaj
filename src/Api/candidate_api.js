import axios from "axios";
import * as SYSTEM_CONST from "../globalConstants";
const token = () => JSON.parse(localStorage.getItem("token"));
import firebase from "firebase/app"
import "firebase/database"

const DUMP_TO_FIREBASE = function (url, method, error) {
  let user = JSON.parse(localStorage.getItem("user"));
  let database = firebase.database();
  let log = {
    url: url,
    method: method,
    status: error.status
  };
  if(error.statusText) {
    log.statusText = error.statusText
  }
  if(error.data) {
    log.data = error.data
  }
  database.ref('web-application/candidates/' + user.user_id).push({
    time : new Date().getTime(),
    log: log
  }).catch()
};
// Invite New People

export const SEND_INVITATION = function sendInvitation(emails) {
  let data = JSON.stringify({
    emails: emails
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.INVITE_PEOPLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
    .then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.INVITE_PEOPLE, SYSTEM_CONST.METHOD.POST, response);
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    })
    .catch(function(err) {
      DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.INVITE_PEOPLE, SYSTEM_CONST.METHOD.POST, err.response);
      if(err.response.data) {
        err.message = err.response.data.description || err.response.data.title;
      } else {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
      }
      reject(err);
    });
  });
};

// Basic Information
export const GET_USER_DEMOGRAPHIC_INFO = function getUserDemographicInfo(info) {
  let params = {};
  if (info) {
    params = info;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const SAVE_USER_DEMOGRAPHIC_INFO = function saveUserDemographicInfo(
  info
) {
  let data = {};
  if (info.user_name !== undefined) {
    data.user_name = info.user_name;
  }
  if (info.location !== undefined) {
    data.location_name = info.location;
  }
  if (info.gender) {
    data.gender = info.gender;
  }
  if (info.profile_status) {
    data.profile_status = info.profile_status;
  }
  if (info.phone !== undefined) {
    data.phone = info.phone;
  }
  if (info.linkedin !== undefined) {
    data.linkedin = info.linkedin;
  }
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL, SYSTEM_CONST.METHOD.PUT, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_DEMOGRAPHIC_INFO_URL, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Educational Information

export const GET_USER_EDUCATIONAL_INFO = function getUserEducationalInfo() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_USER_EDUCATIONAL_INFO = function saveUserEducationalInfo(
  info
) {
  let method = "";
  if (info.attrib_id) {
    method = SYSTEM_CONST.METHOD.PUT;
  } else {
    method = SYSTEM_CONST.METHOD.POST;
  }
  let data = JSON.stringify(info);
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, method, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, method, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_USER_EDUCATIONAL_INFO = function deleteUserEducationalInfo(
  id
) {
  let data = JSON.stringify({
    attrib_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EDUCATIONAL_INFO_URL, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Experience Information

export const GET_USER_EXPERIENCES_INFO = function getUserExperiencesInfo() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_USER_EXPERIENCES_INFO = function saveUserExperiencesInfo(
  info
) {
  let method = "";
  if (info.attrib_id) {
    method = SYSTEM_CONST.METHOD.PUT;
  } else {
    method = SYSTEM_CONST.METHOD.POST;
  }
  let data = JSON.stringify(info);
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, method, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, method, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_USER_EXPERIENCES_INFO = function deleteUserExperiencesInfo(
  id
) {
  let data = JSON.stringify({
    attrib_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_EXPERIENCES_INFO_URL, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Skill Information

export const GET_ALL_SKILLS = function getAllSkills() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_ALL_SKILLS = function saveAllSkills(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify({
        skills: data
      })
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_SKILL = function saveSkill(info) {
  let data = JSON.stringify({
    attrib_id: info.attrib_id,
    skill_level: info.level
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.PUT, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_SKILL = function deleteSkill(info) {
  let data = JSON.stringify({
    attrib_id: info.attrib_id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_SKILLS_INFO_URL, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Project Information

export const GET_CANDIDATE_PROJECTS = function getUserEducationalInfo() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_CANDIDATE_PROJECT = function saveUserEducationalInfo(info) {
  let method = "";
  if (info.attrib_id) {
    method = SYSTEM_CONST.METHOD.PUT;
  } else {
    method = SYSTEM_CONST.METHOD.POST;
  }
  let data = JSON.stringify(info);
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, method, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, method, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_CANDIDATE_PROJECT = function deleteUserEducationalInfo(id) {
  let data = JSON.stringify({
    attrib_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_PROJECTS, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Skill Information

export const GET_ALL_LANGUAGES = function getAllSkills() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_ALL_LANGUAGES = function saveAllSkills(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify({
        skills: data
      })
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_LANGUAGE = function saveSkill(info) {
  let data = JSON.stringify({
    attrib_id: info.attrib_id,
    language_level: info.level
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.PUT, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_LANGUAGE = function deleteSkill(info) {
  let data = JSON.stringify({
    attrib_id: info.attrib_id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_LANGUAGES, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// training information

export const GET_CANDIDATE_TRAININGS = function getUserEducationalInfo() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_CANDIDATE_TRAINING = function saveUserEducationalInfo(info) {
  let method = "";
  if (info.attrib_id) {
    method = SYSTEM_CONST.METHOD.PUT;
  } else {
    method = SYSTEM_CONST.METHOD.POST;
  }
  let data = JSON.stringify(info);
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, method, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, method, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const DELETE_CANDIDATE_TRAINING = function deleteUserEducationalInfo(
  id
) {
  let data = JSON.stringify({
    attrib_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_TRAININGS, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

//candidate filter
export const GET_SAVED_FILTER_DATA = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const SAVE_FILTER = function(data, filter_name) {
  let Data = {
    filters: data,
    filter_name: filter_name
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(Data)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const UPDATE_FILTER = function(data, attrib_id) {
  let Data = {
    filters: data,
    attrib_id: attrib_id
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(Data)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.PUT, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const DELETE_FILTER = function(id) {
  let data = JSON.stringify({
    attrib_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_JOBS, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Search Jobs
export const SEARCH_JOBS_CB = function searchJobs(params, filters) {
  let data = {
    q: params.q,
    offset: params.offset
    // limit: params.limit
  };
  let url = "?q=" + encodeURIComponent(params.q) + "&offset=" + encodeURIComponent(params.offset);
  if (filters) {
    if (filters.position_type && filters.position_type.length > 0) {
      let i,
        temp = "&position_type=";
      for (i = 0; i < filters.position_type.length; i++) {
        temp = temp + encodeURIComponent(filters.position_type[i]);
        if (i < filters.position_type.length - 1) temp = temp + ",";
      }
      url = url + temp;
    }
    if (filters.seniority_level && filters.seniority_level.length > 0) {
      let i,
        temp = "&seniority_level=";
      for (i = 0; i < filters.seniority_level.length; i++) {
        temp = temp + encodeURIComponent(filters.seniority_level[i]);
        if (i < filters.seniority_level.length - 1) temp = temp + ",";
      }
      url = url + temp;
    }
    if (filters.companies && filters.companies.length > 0) {
      let i,
        temp = "&companies=";
      for (i = 0; i < filters.companies.length; i++) {
        filters.companies[i].replace("&", "%26");
        temp = temp + encodeURIComponent(filters.companies[i]);
        if (i < filters.companies.length - 1) temp = temp + ",";
      }
      url = url + temp;
    }
    if (filters.min_salary) {
      url = url + "&min_salary=" + encodeURIComponent(filters.min_salary);
    }
    if (filters.max_salary) {
      url = url + "&max_salary=" + encodeURIComponent(filters.max_salary);
    }
    if (filters.duration !== undefined) {
      url = url + "&duration=" + encodeURIComponent(filters.duration);
    }
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_JOB_SEARCH + url,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_JOB_SEARCH+url, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_JOB_SEARCH+url, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Candidate Action
export const CANDIDATE_ACTION_GET = function getAppliedJob(action, last_key) {
  let params = {
    action: action
  };
  if (last_key) {
    params.last_evaluated_key = last_key;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.data.jobs = response.data.entities;
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};
export const CANDIDATE_ACTION_POST = function apply(id, action) {
  let data = JSON.stringify({
    job_id: id,
    action: action
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.POST, response);
          reject(response);
        }
      })
      .catch(function(err) {
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.POST, err.response);
        reject(err);
      });
  });
};
export const CANDIDATE_ACTION_DELETE = function cancelApplication(id, action) {
  let data = JSON.stringify({
    job_id: id,
    action: action
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_ACTION, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

// Resume

export const GET_RESUME = function getResume() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.RESUME,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const UPLOAD_RESUME = function uploadResume(file, existing) {
  let formData = new FormData();
  formData.append("file", file);
  if (existing) {
    formData.append("attrib_id", existing.attrib_id);
    formData.append("download_key", existing.download_key);
    formData.append("view_key", existing.view_key);
  }
  return new Promise((resolve, reject) => {
    axios
      .post(SYSTEM_CONST.BASE_URL.RESUME, formData, {
        headers: {
          Authorization: "Bearer " + token(),
          "Content-Type": "multipart/form-data"
        }
      })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const DELETE_RESUME = function deleteResume(file) {
  let data = JSON.stringify({
    download_key: file.download_key,
    view_key: file.view_key,
    attrib_id: file.attrib_id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.RESUME,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const RESUME_ACTION = function resumeAction(key, action) {
  let params = {};
  if (action === SYSTEM_CONST.RESUME_ACTION.VIEW) {
    params.key = key;
  } else {
    params.key = key;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.RESUME,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESUME, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const UPLOAD_IMAGE = function uploadImage(file) {
  let formData = new FormData();
  formData.append("file", file);
  return new Promise((resolve, reject) => {
    axios
      .post(SYSTEM_CONST.BASE_URL.IMAGE, formData, {
        headers: {
          Authorization: "Bearer " + token(),
          "Content-Type": "multipart/form-data"
        }
      })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const DELETE_IMAGE = function deleteImage(file) {
  let data = JSON.stringify({
    key: file.s3_key
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.IMAGE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const FOLLOW_COMPANY = function followCompany(id) {
  let data = JSON.stringify({
    company_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const UNFOLLOW_COMPANY = function unfollowCompany(id) {
  let data = JSON.stringify({
    company_id: id
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.DELETE, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const GET_FOLLOWING_COMPANIES = function getCompanies() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.FOLLOW_COMPANIES, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const GET_PROFILE_COMPLETION_STATUS = function getStatus() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.PROFILE_COMPLETION_STATUS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.PROFILE_COMPLETION_STATUS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.PROFILE_COMPLETION_STATUS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const GET_CANDIDATE_DIGEST = function getUserEducationalInfo() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        } else {
          err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        }
        reject(err);
      });
  });
};

export const GET_CANDIDATE_NEWS_FEED = function getUserEducationalInfo(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_NEWS_FEED,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        resolve(response.data);
      } else {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, response);
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, err.response);
      if(err.response.data) {
        err.message = err.response.data.description || err.response.data.title;
      } else {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
      }
      reject(err);
    });
  });
};

export const PIN_NEWS = function getUserEducationalInfo(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_NEWS_FEED,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        resolve(response.data);
      } else {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, response);
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DIGEST, SYSTEM_CONST.METHOD.GET, err.response);
      if(err.response.data) {
        err.message = err.response.data.description || err.response.data.title;
      } else {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
      }
      reject(err);
    });
  });
};