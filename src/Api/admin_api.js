import axios from "axios";
import * as SYSTEM_CONST from "../globalConstants";
const token = () => JSON.parse(localStorage.getItem("token"));
//employer filter
const JobStatus = {
  init: 1,
  draft: 2,
  published: 3,
  approved: 4,
  closed: 5,
  discarded: 6,
  pending: 7
};

const JobAction = {
  save: 1,
  discard: 2,
  publish: 3,
  close: 4,
  approve: 5,
  draftEdit: 6,
  pendingEdit: 7,
  publishEdit: 8
};

export const GET_JOBS_ADMIN = function(params) {
  let Data = {
    status: JobStatus[params.status]
  };
  if (params.last_evaluated_key) {
    Data.last_evaluated_key = params.last_evaluated_key;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.ADMIN_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const CHANGE_JOB_STATUS = function(params) {
  let Data = {
    job_ids: params.job_ids,
    action: params.action
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.ADMIN_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: Data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const GET_USERS_ADMIN = function(sub, featured) {
  let params = {};
  let url = SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_USERS;
  if (sub) {
    params = sub;
  }
  if (featured) {
    url = url + "/filter";
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: url,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const CHANGE_USER_STATUS = function(params) {
  let Data = {
    company_id: params.company_id
  };
  if(params.override) {
    Data.override = true;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.ADMIN_REGISTER_COMPANY,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: Data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const UPDATE_USER_STATUS = function(params) {
  let Data = {
    company_id: params.company_id,
    plan_id: params.plan_id
  };
  if (params.discount) Data.discount = params.discount;
  if (params.payment_status) Data.payment_status = params.payment_status;
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: Data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const GET_SUBSCRIPTION_PLANS = function(type, status) {
  let params = {};
  if (type) {
    params.plan_type = type;
  }
  if (status) {
    params.plan_status = status;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_FEATURES = function() {
  let params = {
    features_only: true
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};
export const ADD_PLAN = function(plan) {
  let params = {
    plan_name: plan.plan_name,
    duration_in_days: plan.duration_in_days,
    price: plan.price,
    plan_status: plan.plan_status,
    attributes: plan.attributes
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const UPDATE_PLAN = function(plan) {
  let params = {
    plan_id: plan.plan_id,
    plan_name: plan.plan_name,
    duration_in_days: plan.duration_in_days,
    price: plan.price,
    plan_status: plan.plan_status,
    attributes: plan.attributes
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const INVOICE_ACTION = function(key) {
  let params = {
    key: key
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.INVOICE_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const MAIL_INVOICE = function(id) {
  let params = {
    company_id: id,
    send_invoice: true
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.GET_PLANS_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch(function(err) {
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};

export const AUTO_SUGGEST_COMPANY_COMPLIANCE = function(key) {
  let params = {
    q: key
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.ADMIN_AUTO_SUGGEST_COMPANY_COMPLIANCE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const ADMIN_POST_A_JOB = function(params) {
  if (params.action) params.action = JobAction[params.action];
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.ADMIN_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(err);
      });
  });
};

export const ADMIN_MAKE_FEATURED_COMPANY = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.MAKE_FEATURED,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(err);
      });
  });
};

export const HANDOVER_COMPANY = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_USERS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
    .then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    })
    .catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const CREATE_NEW_COMPANY = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_USERS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
    .then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    })
    .catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const DELETE_COMPANY = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_USERS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
    .then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    })
    .catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const GET_NEWS_ADMIN = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.ADMIN_NEWS_MANAGEMENT,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
    .then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    })
    .catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const APPROVE_NEWS_ADMIN = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.ADMIN_NEWS_MANAGEMENT + '/approve',
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    }).then(function(response) {
          if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
            response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
            resolve(response);
          } else {
            response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
            reject(response);
          }
    }).catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const DELETE_NEWS_ADMIN = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.ADMIN_NEWS_MANAGEMENT,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const GET_ALL_ADMINS = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const CRETAE_ADMIN = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(data)
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};

export const GET_NEWS_SUPER_ADMIN = function(params) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.SUPER_ADMIN_MANAGE_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }}).catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(err);
      });
  });
};

export const UPDATE_ADMIN = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.ADMIN_MANAGE_ADMIN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(data)
    }).then(function(response) {
      if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
        resolve(response);
      } else {
        response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
        reject(response);
      }
    }).catch(function(err) {
      err.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
      reject(err);
    });
  });
};