import axios from "axios";
import * as SYSTEM_CONST from "../globalConstants";
const token = () => JSON.parse(localStorage.getItem("token"));
import firebase from "firebase/app"
import "firebase/database"

const DUMP_TO_FIREBASE = function (url, method, error) {
  let user = JSON.parse(localStorage.getItem("user"));
  let database = firebase.database();
  let log = {
    url: url,
    method: method,
    status: error.status
  };
  if(error.statusText) {
    log.statusText = error.statusText
  }
  if(error.data) {
    log.data = error.data
  }
  if(user.user_type === 3) {
    database.ref('web-application/candidates/' + user.user_id).push({
      time : new Date().getTime(),
      log: log
    }).catch()
  } else if(user.user_type === 2) {
    database.ref('web-application/employers/' + user.user_id).push({
      time : new Date().getTime(),
      log: log
    }).catch()
  } else if(user.user_type === 1) {
    database.ref('web-application/admin/' + user.user_id).push({
      time : new Date().getTime(),
      log: log
    }).catch()
  } else {
    database.ref('web-application/public/').push({
      time : new Date().getTime(),
      log: log
    }).catch()
  }
};

export const SIGN_IN_SUCCESS = function signInSuccess(token) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.SIGN_IN_SUCC,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token
      }
    })
    .then(resp => {
      resolve(resp);
    })
    .catch(err => {
      DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SIGN_IN_SUCC, SYSTEM_CONST.METHOD.GET, err.response);
      reject(err);
    });
  });
};

export const UPDATE_PASSWORD = function updatePassword(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(data)
    })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL, SYSTEM_CONST.METHOD.PUT, err.response);
        if (err.response && err.response.status === 406) {
          resolve(err.response);
        } else reject(err);
      });
  });
};

export const GET_AUTO_SUGGEST = function(params, type, not_following) {
  let data = {};
  if (params) {
    data.q = params;
  }
  if (not_following) {
    data.not_following = 1;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.AUTO_SUGGESTION_GET_URL + type + "/",
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.AUTO_SUGGESTION_GET_URL+type, SYSTEM_CONST.METHOD.PUT, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.AUTO_SUGGESTION_GET_URL+type, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const VALIDATE_TOKEN = function tokenExpireCallBack(token, type) {
  let params = {};
  if (type) {
    params.prefix = type;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.VALIDATE_TOKEN,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token
      },
      params: params
    })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.VALIDATE_TOKEN, SYSTEM_CONST.METHOD.GET, err.response);
        if (err.response && err.response.status === 406) {
          resolve(err.response);
        } else reject(err);
      });
  });
};

export const RESEND_EMAIL = function(info) {
  let data = JSON.stringify({
    email: info.email,
    email_type: info.email_type,
    token: info.token
  });
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.RESEND_EMAIL_API_URL,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
      },
      data: data
    })
      .then(resp => {
        if (resp.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resp.message = SYSTEM_CONST.RESEND_EMAIL.SUCCESS;
          resolve(resp);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESEND_EMAIL_API_URL, SYSTEM_CONST.METHOD.POST, resp);
          reject(resp);
        }
      })
      .catch(err => {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.RESEND_EMAIL_API_URL, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const IMAGE = function profileImage(info) {
  let params = {};
  if (info) {
    params = info;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.USER_IMAGE_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_IMAGE_ACTION, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.USER_IMAGE_ACTION, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const JOBS_FROM_SAME_COMPANY = function(id, last_key) {
  let Data = {
    company_id: id
  };
  if (last_key) {
    Data.last_evaluated_key = last_key;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.JOBS_FROM_SAME_COMPANY,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.JOBS_FROM_SAME_COMPANY, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.JOBS_FROM_SAME_COMPANY, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const GET_JOB_DETAILS = function(id,trending=false) {
  let data = {
    job_id: id
  };
  trending&&(data.source="search");
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.GET_JOB_DETAILS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: data
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.GET_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.GET_JOB_DETAILS, SYSTEM_CONST.METHOD.GET, err.esponse);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const GET_ANALYTICS = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.ANALYTICS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.ANALYTICS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.ANALYTICS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_ALL_THREADS = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.MESSAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        err.message = SYSTEM_CONST.ERROR_MESSAGES.SERVER_DOWN;
        reject(err);
      });
  });
};

export const GET_MESSAGES_BY_THREAD_ID = function(id) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.MESSAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        thread_id: id
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_COUNT_UPDATE = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.MESSAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        count_only: true
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_MESSAGE_SUBJECT = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.MESSAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        subjects_only: true
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const SEND_NEW_MESSAGE = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.MESSAGES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(data)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.POST, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.MESSAGES, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const DOCUMENT_ACTION = function resumeAction(key, action) {
  let params = {};
  if (action === SYSTEM_CONST.RESUME_ACTION.VIEW) {
    params.key = key;
  } else {
    params.key = key;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_COMPANIES_SEARCH = function resumeAction(q) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_COMPANIES,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        q: q
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_COMPANIES, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_FILTER_COMPANIES, SYSTEM_CONST.METHOD.GET, err.response);
        if (err.response.status === 404) resolve(err.response);
        else {
          if(err.response.data) {
            err.message = err.response.data.description || err.response.data.title;
          }
          reject(err);
        }
      });
  });
};

export const IMPORT_JOB_USING_URL = function importAction(q) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_JOB_IMPORT,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        job_url: encodeURI(q)
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOB_IMPORT, SYSTEM_CONST.METHOD.GET, response);
          response.message = SYSTEM_CONST.ERROR_MESSAGES.DEFAULT;
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOB_IMPORT, SYSTEM_CONST.METHOD.GET, err.response);
        if (err.response.status === 404) resolve(err.response);
        else {
          if(err.response.data) {
            err.message = err.response.data.description || err.response.data.title;
          }
          reject(err);
        }
      });
  });
};
