import axios from "axios";
import * as SYSTEM_CONST from "../globalConstants";
import firebase from "firebase/app"
import "firebase/database"
const token = () => JSON.parse(localStorage.getItem("token"));
//employer filter
const JobStatus = {
  init: 1,
  draft: 2,
  published: 3,
  approved: 4,
  closed: 5,
  discarded: 6,
  pending: 7
};
const JobAction = {
  save: 1,
  discard: 2,
  publish: 3,
  close: 4,
  approve: 5,
  draftEdit: 6,
  pendingEdit: 7,
  publishEdit: 8
};
const EmployerActionType = {
  1: "favorite",
  2: "rejected"
};

const DUMP_TO_FIREBASE = function (url, method, error) {
  let user = JSON.parse(localStorage.getItem("user"));
  let database = firebase.database();
  let log = {
    url: url,
    method: method,
    status: error.status
  };
  if(error.statusText) {
    log.statusText = error.statusText
  }
  if(error.data) {
    log.data = error.data
  }
  database.ref('web-application/employers/' + user.user_id).push({
    time : new Date().getTime(),
    log: log
  }).catch()
};


export const FILTER_ACTION = function(filters, offset, job_id, company_id) {
  let url = "";
  if (filters) {
    for (let key in filters) {
      if (!filters.hasOwnProperty(key)) continue;
      let obj = filters[key];
      let temp = "";
      url = url + "&" + key + "=";
      for (let prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        temp = temp + encodeURIComponent(obj[prop]);
        if (prop < obj.length - 1) temp = temp + ",";
      }
      url = url + temp;
    }
    url = SYSTEM_CONST.BASE_URL.APPLICANT_FILTER + "?job_id=" + job_id + "&offset=" + offset + url;
    if(company_id) {
      url =url + "&company_id=" + company_id
    }
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: url,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(url, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const GET_FILTER_DATA = function(info) {
  let Data = {
    job_id: info.job_id
  };
  if(info.company_id) {
    Data.company_id = info.company_id
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.GET_FILTER_DATA,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        resolve(response.data);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.GET_FILTER_DATA, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const USER_FILTER = function(params, type) {
  let Data = {};
  Data.filter_name = params.filter_name;
  if (params.skills.length > 0) Data.skills = params.skills;
  if (params.expValue.length > 0) Data.exp_lvl = params.expValue;
  if (params.degrees.length > 0) Data.degrees = params.degrees;
  if (params.institutions.length > 0) Data.institutes = params.institutions;
  return new Promise((resolve, reject) => {
    axios({
      method:
        type === "save" ? SYSTEM_CONST.METHOD.POST : SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_SIDE_FILTER,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(Data)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_SIDE_FILTER, SYSTEM_CONST.METHOD.POST, err.response);
        if (err.status) {
          resolve(err);
        } else {
          reject(err);
        }
      });
  });
};
//Candidate Search
export const CANDIDATE_SEARCH = function(params, filter) {
  let Data = {
    q: params.q,
    offset: params.offset
    // limit: params.limit
  };
  if (filter) {
    let i,
      institution = "",
      degrees = "",
      skills = "",
      exp_lvl = "";
    if (filter.exp_lvl) {
      exp_lvl = exp_lvl + filter.exp_lvl.join(",") + ",";
      Data.exp_lvl = exp_lvl;
    }

    if (filter.skills) {
      for (i = 0; i < filter.skills.length; i++) {
        skills = skills + filter.skills[i].skill_name + ",";
      }
      Data.skills = skills;
    }
    if (filter.degrees) {
      for (i = 0; i < filter.degrees.length; i++) {
        degrees = degrees + filter.degrees[i].degree + ",";
      }
      Data.degrees = degrees;
    }
    if (filter.institutes) {
      for (i = 0; i < filter.institutes.length; i++) {
        institution = institution + filter.institutes[i].institution + ",";
      }
      Data.institutes = institution;
    }
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const CANDIDATE_SEARCH_VALIDATE = function() {
  let params = {
    validate_search_action: true
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === 204) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SEARCH_CANDIDATE, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const CANDIDATE_DETAILS = function(data, jobId) {
  let params = {
    user_id: data.user_id
  };
  if (jobId) {
    params.job_id = jobId;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.CANDIDATE_DETAILS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DETAILS, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.CANDIDATE_DETAILS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

// active/draft/expired Jobs//if Data is empty then it should return all posted jobs
export const JOB = function(params) {
  let Data = {
    status: JobStatus[params.status],
    last_evaluated_key: params.key
  };
  if(params.company_id) {
    Data.company_id = params.company_id
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const CLOSE_ACTIVE_JOB = function(params) {
  if (params.action) params.action = JobAction[params.action];
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS, SYSTEM_CONST.METHOD.PUT, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
//pending jobs
export const GET_ALL_APPLICANTS = function(params) {
  let Data = {
    job_id: params.job_id,
    last_evaluated_key: params.last_evaluated_key
  };
  if(params.company_id) {
    Data.company_id = params.company_id
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.JOB_APPLICANTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.JOB_APPLICANTS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const GET_TYPED_APPLICANTS = function(params) {
  let Data = {
    job_id: params.job_id
  };
  if (params.action_type === "favorite") {
    Data.action_type = 1;
  }
  if (params.action_type === "rejected") {
    Data.action_type = 2;
  }
  if (params.offset) {
    Data.offset = params.offset;
  }
  if (params.company_id) {
    Data.company_id = params.company_id;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: Data
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ACTION, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
export const ADD_OR_REMOVE_APPLICANTS = function(params, action) {
  let Data = {
    job_id: params.job_id,
    action_type: EmployerActionType[params.action_type],
    user_id: params.user_id
  };
  return new Promise((resolve, reject) => {
    axios({
      method:
        action === "add"
          ? SYSTEM_CONST.METHOD.POST
          : SYSTEM_CONST.METHOD.DELETE,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ACTION,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(Data)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ACTION, action === "add" ? SYSTEM_CONST.METHOD.POST : SYSTEM_CONST.METHOD.DELETE, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
//post a job
export const POST_A_JOB = function(params, method) {
  if (params.action) params.action = JobAction[params.action];
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_JOBS, method, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
//user role

export const EMPLOYER_ROLE_ACTION_GET = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const EMPLOYER_ROLE_ACTION_POST = function(user) {
  let params = {
    users: user
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.POST,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const EMPLOYER_ROLE_ACTION_PUT = function(user) {
  let params = user;
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const EMPLOYER_PLAN_MANAGEMENT = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const UPDATE_EMPLOYER_PLAN_MANAGEMENT = function(param) {
  let params = {
    company_id: param.company_id,
    plan_id: param.plan_id
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(params)
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT, SYSTEM_CONST.METHOD.PUT, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_PLAN_MANAGEMENT, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const EMPLOYER_UPDATE_COMPANY_INFO = function(data) {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.PUT,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      data: JSON.stringify(data)
    })
      .then(function(response) {
        resolve(response);
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.PUT, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET__COMPANY_INFO = function() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: {
        emp_action: "get_company_info"
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_ROLE, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const UPLOAD_ALL_DOCUMENTS = function uploadResume(nid, tin, tl) {
  let formData = new FormData();
  formData.append("nid", nid);
  formData.append("tin", tin);
  formData.append("tl", tl);
  return new Promise((resolve, reject) => {
    axios
      .post(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, formData, {
        headers: {
          Authorization: "Bearer " + token(),
          "Content-Type": "multipart/form-data"
        }
      })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          response.message = SYSTEM_CONST.ERROR_MESSAGES.UPDATED;
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.POST, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.POST, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_ALL_LEGAL_DOCUMENTS = function uploadResume() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    }).then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
    }).catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_UPLOAD_DOCUMENTS, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
    });
  });
};

export const GET_ALL_APPLICANTS_CV_BANK = function getApplicants(user_id) {
  let params = {};
  if (user_id) {
    params.user_id = user_id;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_ALL_APPLICANTS_CV_BANK_PAGINATION = function getApplicants(
  offset
) {
  let params = {
    offset: offset
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const GET_CV_BANK_FILTER_OPTIONS = function getApplicants() {
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_FILTER,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS) {
          resolve(response.data);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_FILTER, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_FILTER, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const FILTER_CV_BANK = function searchJobs(filters, offset) {
  let url = "";
  if (filters) {
    for (let key in filters) {
      if (!filters.hasOwnProperty(key)) continue;
      let obj = filters[key];
      let temp = "";
      url = url + "&" + key + "=";
      for (let prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        temp = temp + encodeURIComponent(obj[prop]);
        if (prop < obj.length - 1) temp = temp + ",";
      }
      url = url + temp;
    }
    url =
      SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_FILTER_ELEM +
      "offset=" +
      offset +
      url;
  }
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: url,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      }
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(url, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(url, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};

export const SEARCH_CV_BANK = function getApplicants(q, offset) {
  let params = {
    q: q,
    offset: offset
  };
  return new Promise((resolve, reject) => {
    axios({
      method: SYSTEM_CONST.METHOD.GET,
      url: SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_SEARCH,
      headers: {
        "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
        Authorization: "Bearer " + token()
      },
      params: params
    })
      .then(function(response) {
        if (
          response.status === SYSTEM_CONST.RESPONSE_CODE.SUCCESS ||
          response.status === SYSTEM_CONST.RESPONSE_CODE.PROCESSING
        ) {
          resolve(response);
        } else {
          DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_SEARCH, SYSTEM_CONST.METHOD.GET, response);
          reject(response);
        }
      })
      .catch(function(err) {
        DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_CV_BANK_SEARCH, SYSTEM_CONST.METHOD.GET, err.response);
        if(err.response.data) {
          err.message = err.response.data.description || err.response.data.title;
        }
        reject(err);
      });
  });
};
