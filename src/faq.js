export const FAQ_CANDIDATES = [
  {
    question: "1. Do I need to complete my profile?",
    answer:
      "You can apply for jobs without completing your profile. " +
      "However, the employers are less likely to consider you with an incomplete profile. " +
      "We recommend that you complete 100% of your profile to maximize your chances of securing a job."
  },
  {
    question: "2. How do I apply for a job?",
    answer:
      "Please sign up on our website and complete your profile. " +
      "Then you will find a wide range of job opportunities in our dashboard, click apply for the jobs you are interested in."
  },
  {
    question: "3. How long does it take to get a job?",
    answer:
      "Our platform generates a unique matching profile for each candidate based on their choices, searches, and queries. " +
      "But it is not possible to estimate the time required to get a job." +
      " However, the completion of your profile will certainly increase your chances of getting hired."
  },
  {
    question: "4. What kind of jobs are available?",
    answer:
      "We have a wide range of jobs available from different sectors such as IT, marketing, sales, accounts, graphic designing, engineering, etc. " +
      "Apart from fulltime jobs, we also have jobs suited for candidates looking for part-time or work from jobs."
  },
  {
    question: "5. Can a student apply for a job?",
    answer:
      "Students with an account on our website are eligible for jobs matching their skillset. " +
      "Our platform generates a unique matching profile for each candidate based on their profile, choices, searches, and queries. Students can also manually apply for any job from our dashboard."
  },
  {
    question: "6. Do I need a degree to apply for a job?",
    answer:
      "Any individual with a profile on our website is eligible to apply for any job. " +
      "However, we recommend reading the job description before applying to understand the requirements of the employers. " +
      "Uploading your credentials such as experiences, educational background, and skills will improve your chances of getting hired. "
  },
  {
    question: "7. How do I increase my chances of getting hired?",
    answer:
      "We recommend completing your profile and uploading your credentials such as experiences, educational background, and skills to improve your chances of getting hired."
  },
  {
    question: "8. How is workd different from conventional job sites?",
    answer:
      "Workd uses intuitive search-and-apply features to filter jobs and effortlessly connect applicants to their suitable jobs with one click. " +
      "The most notable feature of our platform is that we generate a unique matching profile for each candidate based on their choices, searches, and queries. "
  }
];

export const FAQ_EMPLOYERS = [
  {
    question: "1. What are the conditions for reposting a job?",
    answer:
      "Once your job post has expired you can choose to repost it anytime you want. "
  },
  {
    question: "2. How many jobs can I keep as a draft?",
    answer: "You can create unlimited number of drafts from your account."
  },
  {
    question: "3. Am I able to edit any of my live jobs ?",
    answer:
      "Once you have uploaded your job post, you cannot edit  until it has expired. " +
      "However, you can always take down your current job post, make the necessary changes and upload it afterwards."
  },
  {
    question: "4. Is it possible to change company name, if yes then how?",
    answer:
      "Once you have registered an account with your company name you cannot change it again."
  },
  {
    question: "5. How long will a job remain live on the website?",
    answer:
      "A job post remains live on our dashboard for 30days before expiring."
  },
  {
    question:
      "6. Is it possible to post a job without mentioning my company name?",
    answer:
      "Your account is registered to your company name. So you cannot upload a job post without mentioning the company name."
  },
  {
    question: "7. How do I repost a job?",
    answer:
      "First login to your account and press on the job section on the left side of your screen. There you will see five more sub sections, click on expired and then click on the job you want to repost. " +
      "Afterwards click on details on the top right side of the screen and press re-post."
  },
  {
    question: "8. Is it possible to extend the time period of a live job?",
    answer:
      "You can extend or shorten your live job post within the default 30 days."
  }
];
