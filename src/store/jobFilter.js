export default FilterStore={
    state:{
        seniority_level:[],
        position_type:[],
        companies:[],
        min_salary:"",
        max_salary:"",
        duration:""
    },
    mutations:{
        addSeniority:(state,val)=>state.seniority_level.push(val),
        addPostion:(state,val)=>state.position_type.push(val),
        addCompanies:(state,val)=>state.companies.push(val),
        addMinSalary:(state,val)=>state.min_salary=val,
        addMaxSalary:(state,val)=>state.max_salary=val,
        addDuration:(state,val)=>state.duration=val,

        removeSeniority:(state,val)=>state.seniority_level.splice(indexOf(val),1),
        removePostion:(state,val)=>state.position_type.splice(indexOf(val),1),
        removeCompanies:(state,val)=>state.position_type.splice(indexOf(val),1),
        removeMinSalary:(state,val)=>state.min_salary="",
        removeMaxSalary:(state,val)=>state.max_salary="",
        removeDuration:(state,val)=>state.duration=""
    },
    getters:{
        getAllFilter:(state)=>state
    }
}