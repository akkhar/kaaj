import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import * as SYSTEM_CONST from "../globalConstants";
import contactForm from "./contactForm";
import contactFormStore from "./contactForm";
import { SIGN_IN_SUCCESS } from "../Api/common_api";
import firebase from "firebase/app"
import "firebase/database"
import {UserInfo} from "./userinfo"

const DUMP_TO_FIREBASE = function (url, method, error) {
  let database = firebase.database();
  let log = {
    url: url,
    method: method,
    status: error.status
  };
  if(error.statusText) {
    log.statusText = error.statusText
  }
  if(error.data) {
    log.data = error.data
  }
  database.ref('web-application/login/').push({
    time : new Date().getTime(),
    log: log
  }).catch()
};

Vue.use(Vuex);

const Session = {
  state: {
    token: localStorage.getItem("token") || ""
  },
  mutations: {
    auth_success(state, token) {
      state.token = token;
      localStorage.setItem("message", JSON.stringify("Logged in"));
    },
    auth_error(state) {
      state.token = "";
    },
    logout(state) {
      state.token = "";
      localStorage.clear();
      localStorage.setItem("message", JSON.stringify("Logged out"));
    }
  },
  actions: {
    signIn({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({
          method: SYSTEM_CONST.METHOD.POST,
          url: SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
          },
          data: user
        })
          .then(resp => {
            let dTok = resp.data.token;
            SIGN_IN_SUCCESS(dTok)
              .then(function(response) {
                const token = JSON.stringify(response.data.token);
                const user = JSON.stringify(response.data);
                localStorage.setItem("token", token);
                localStorage.setItem("user", user);
               
                commit("auth_success", token);
                SYSTEM_CONST.setCandidateInfo();
                resolve(response);
              })
              .catch(function(err) {
                commit("auth_error");
                localStorage.clear();
                if (err.response) {
                  resolve(err.response);
                } else {
                  reject(err);
                }
              });
          })
          .catch(err => {
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL, SYSTEM_CONST.METHOD.POST, err.response);
            commit("auth_error");
            localStorage.clear();
            if (err.response) {
              resolve(err.response);
            } else {
              reject(err);
            }
          });
      });
    },
    signUp({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({
          method: SYSTEM_CONST.METHOD.POST,
          url: SYSTEM_CONST.BASE_URL.SIGN_UP_API_URL,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
          },
          data: JSON.stringify(user)
        })
          .then(resp => {
            let dTok = resp.data.token;
            SIGN_IN_SUCCESS(dTok).then(function(response) {
              const token = JSON.stringify(response.data.token);
              const user = JSON.stringify(response.data);
              Vue.$ga.set("userId", response.data.user_id);
              localStorage.setItem("token", token);
              localStorage.setItem("user", user);
              commit("auth_success", token);
              SYSTEM_CONST.setCandidateInfo();
              resolve(response);
            });
          })
          .catch(err => {
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SIGN_UP_API_URL, SYSTEM_CONST.METHOD.POST, err.response);
            commit("auth_error");
            reject(err);
          });
      });
    },
    oAuthSignUp({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({
          method: SYSTEM_CONST.METHOD.POST,
          url: SYSTEM_CONST.BASE_URL.OAUTH_SIGN_UP,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
          },
          data: JSON.stringify(user)
        })
          .then(resp => {
            let dTok = resp.data.token;
            SIGN_IN_SUCCESS(dTok).then(function(response) {
              const token = JSON.stringify(response.data.token);
              const user = JSON.stringify(response.data);
              Vue.$ga.set("userId", response.data.user_id);
              localStorage.setItem("token", token);
              localStorage.setItem("user", user);
              commit("auth_success", token);
              SYSTEM_CONST.setCandidateInfo();
              resolve(response);
            });
          })
          .catch(err => {
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.OAUTH_SIGN_UP, SYSTEM_CONST.METHOD.POST, err.response);
            commit("auth_error");
            reject(err);
          });
      });
    },
    empSignUp({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios({
          method: SYSTEM_CONST.METHOD.POST,
          url: SYSTEM_CONST.BASE_URL.EMPLOYER_SIGN_UP,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON
          },
          data: JSON.stringify(user)
        })
          .then(resp => {
            let dTok = resp.data.token;
            SIGN_IN_SUCCESS(dTok).then(function(response) {
              const token = JSON.stringify(response.data.token);
              const user = JSON.stringify(response.data);
              Vue.$ga.set("userId", response.data.user_id);
              localStorage.setItem("token", token);
              localStorage.setItem("user", user);
              commit("auth_success", token);
              SYSTEM_CONST.setCandidateInfo();
              resolve(response);
            });
          })
          .catch(err => {
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.EMPLOYER_SIGN_UP, SYSTEM_CONST.METHOD.POST, err.response);
            commit("auth_error");
            reject(err);
          });
      });
    },
    signOut({ commit }) {
      return new Promise((resolve, reject) => {
        let user = SYSTEM_CONST.getOwnerInfo();
        axios({
          method: SYSTEM_CONST.METHOD.DELETE,
          url: SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
            Authorization: "Bearer " + user.token
          },
          data: JSON.stringify({ user_id: user.user_id })
        })
          .then(resp => {
            commit("logout");
            resolve(resp);
          })
          .catch(err => {
            commit("logout");
            commit("auth_error", err);
            if (err.response) {
              resolve(err.response);
            } else {
              reject(err);
            }
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.SIGN_IN_API_URL, SYSTEM_CONST.METHOD.DELETE, err.response);
          });
      });
    },
    tokenExpireCallBack({ commit }) {
      return new Promise((resolve, reject) => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios({
          method: SYSTEM_CONST.METHOD.GET,
          url: SYSTEM_CONST.BASE_URL.VALIDATE_TOKEN,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
            Authorization: "Bearer " + token
          }
        })
          .then(resp => {
            resolve(resp);
          })
          .catch(err => {
            DUMP_TO_FIREBASE(SYSTEM_CONST.BASE_URL.VALIDATE_TOKEN, SYSTEM_CONST.METHOD.GET, err.response);
            console.log(err)
            localStorage.clear();
            commit("auth_error");
            if (err.response) {
              resolve(err.response);
            } else {
              reject(err);
            }
          });
      });
    }
  },
  getters: {
    haveToken: state => !!state.token
  }
};

const ThreadStore = {
  state: {
    req_status: "",
    threads: [],
    total_unread_count: 0,
    total_message_count: 0
  },
  mutations: {
    req_start(state) {
      state.req_status = "loading";
    },
    req_success(state, threads) {
      state.threads = [];
      state.threads = threads;
      state.req_status = "success";
    },
    req_error(state) {
      state.req_status = "failure";
      state.threads = [];
    },
    count_update(state, data) {
      state.total_unread_count = data.total_unread_count;
      state.total_message_count = data.total_message_count;
    }
  },
  actions: {
    getAlltheThreads({ commit }) {
      commit("req_start");
      return new Promise((resolve, reject) => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios({
          method: SYSTEM_CONST.METHOD.GET,
          url: SYSTEM_CONST.BASE_URL.MESSAGES,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
            Authorization: "Bearer " + token
          }
        })
          .then(resp => {
            commit("req_success", resp.data);
            resolve(resp.data);
          })
          .catch(err => {
            commit("req_error");
            reject(err);
          });
      });
    },
    getCountUpdate({ commit }) {
      commit("req_start");
      return new Promise((resolve, reject) => {
        let token = JSON.parse(localStorage.getItem("token"));
        axios({
          method: SYSTEM_CONST.METHOD.GET,
          url: SYSTEM_CONST.BASE_URL.MESSAGES,
          headers: {
            "content-type": SYSTEM_CONST.HEADERS.TYPE.APPLICATION_JSON,
            Authorization: "Bearer " + token
          },
          params: {
            count_only: true
          }
        })
          .then(resp => {
            commit("count_update", resp.data);
            resolve(resp.data);
          })
          .catch(err => {
            commit("req_error");
            reject(err);
          });
      });
    }
  },
  getters: {
    getReqStatus(state) {
      return state.req_status;
    },
    getThreads(state) {
      return state.threads;
    },
    getCount(state) {
      return {
        total_unread_count: state.total_unread_count,
        total_message_count: state.total_message_count
      };
    }
  }
};

const store = new Vuex.Store({
  modules: {
    session: Session,
    thread: ThreadStore,
    contact: contactForm,
    userinfo: UserInfo
  }
});

export default store;
