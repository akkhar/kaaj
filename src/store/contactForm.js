const contactFormStore = {
  state: {
    name: "",
    phone: "",
    email: "",
    reason: "",
    subject: "",
    message: ""
  },
  mutations: {
    changeName: (state, val) => (state.name = val),
    changeEmail: (state, val) => (state.email = val),
    changePhone: (state, val) => (state.phone = val),
    changeReason: (state, val) => (state.reason = val),
    changeSubject: (state, val) => (state.phone = val),
    changeMessage: (state, val) => (state.phone = val)
  },
  actions: {
    changeName: ({ commit }, val) => commit(changeName, val),
    changeEmail: ({ commit }, val) => commit(changeEmail, val),
    changePhone: ({ commit }, val) => commit(changeName, val),
    changeReason: ({ commit }, val) => commit(changeName, val),
    changeSubject: ({ commit }, val) => commit(changeName, val),
    changeMessage: ({ commit }, val) => commit(changeName, val)
  },
  getters: {
    nameState: state => state.name,
    emailState: state => state.email,
    phoneState: state => state.phone,
    reasonState: state => state.reason,
    subjectState: state => state.subject,
    messageState: state => state.message,
    allState: state => state
  }
};
export default contactFormStore;
