export const UserInfo={
    state:{
        username:"",
        profileStatus:""
    },
    mutations:{
        changeUserName:(state,val)=>state.username=val,
        changeStatus:(state,val)=>state.profileStatus=val
    },
    // actions:{
    //     nameChangeAction:()=>
    // },
    getters:{
        getName:(state)=>state.username,
        getStatus:(state)=>state.profileStatus
    }
}