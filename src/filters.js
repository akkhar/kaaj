export const REMOVE_WHITE_SPACES = function removeWhiteSpaces(text) {
  return text.replace(/^\s+|\s+$/g, "");
};

export const FIRST_LETTER_CAPITAL = function makeFirstLetterCapital(text) {
  return text.charAt(0).toUpperCase() + text.slice(1);
};

/**
 * @return {boolean}
 */
export const EMAIL_VALIDATOR = function(email) {
  // eslint-disable-next-line
  let email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return email_re.test(email);
};

export const SORT_PROFILE = (
  data,
  currentlyDoingName = "is_working",
  date = ["begin_date", "end_date"]
) => {
  let current = [];
  let temp = [];
  let final = [];
  data.map(val => {
    val[currentlyDoingName] === 1 ? current.push(val) : temp.push(val);
  });
  current.length > 1 &&
    current.sort((a, b) =>
      a[date[0]].substring(3, 7) > b[date[0]].substring(3, 7) ? 1 : -1
    );
  temp.length > 1 &&
    temp.sort((a, b) => {
        if(a[date[1]].substring(3, 7) === b[date[1]].substring(3, 7)) {
          if(a[date[1]].substring(0,2) === b[date[1]].substring(0,2)) {
            if(a[date[0]].substring(3,7) === b[date[0]].substring(3,7)) {
              return a[date[0]].substring(0,2) > b[date[0]].substring(0,2) ? -1 : 1
            } else {
              return a[date[0]].substring(3,7) > b[date[0]].substring(3,7) ? -1 : 1
            }
          } else return a[date[1]].substring(0,2) > b[date[1]].substring(0,2) ? -1 : 1
        } else {
          return a[date[1]].substring(3, 7) > b[date[1]].substring(3, 7) ? -1 : 1
        }
    }
    );
  final = current.concat(temp);
  return final;
};

export const SORT_PROFILE_FORMAT2 = (
  data,
  currentlyDoingName = "is_working",
  date = ["begin_date", "end_date"]
) => {
  let current = [];
  let temp = [];
  let final = [];
  data.map(val => {
    val[currentlyDoingName] === 1 ? current.push(val) : temp.push(val);
  });
  current.length > 1 &&
    current.sort((a, b) => (a[date[0]] > b[date[0]] ? 1 : -1));
  temp.length > 1 && temp.sort((a, b) => {
    if(a[date[1]] === b[date[1]]) {
      if(a['end_month'] === b['end_month']) {
        if(a[date[0]] === b[date[0]]) {
          return a['start_month'] > b['start_month'] ? -1 : 1
        } else {
          return a[date[0]] > b[date[0]] ? -1 : 1
        }
      } else return a['end_month'] > b['end_month'] ? -1 : 1
    } else {
      return a[date[1]] > b[date[1]] ? -1 : 1
    }
  });
  final = current.concat(temp);
  return final;
};
export const REMOVE_HTML_TAGS = function removeTags(str) {
  if (str === null || str === "") return str;
  else str = str.toString().replace("↵", " ");
  return str.replace(/(<([^>]+)>)/gi, "");
};
