export const OPERATOR = {
  ">=": "more than",
  "<=": "up to"
};

export const PAYMENT_STATUS = {
  1: "DUE",
  2: "ON-REQUEST",
  3: "PAID"
};
