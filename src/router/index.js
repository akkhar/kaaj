import Vue from "vue";
import Router from "vue-router";
import store from "../store/store";

import Confirmation from "@/components/confirmation-page/confirmation.vue";

/*  Landing Pages  */
const LandingPage = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/landing-page.vue");
const PageNotFound = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/pageNotFound");
const IndexMiddle = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/home.vue");
const IndexAboutUs = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/about-us.vue");
const IndexContactUs = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/contact-us.vue");
const IndexSignIn = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/sign-in.vue");
const IndexSignUp = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/sign-up.vue");
const ResetPassword = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/reset-password-page/reset-password.vue");
const EmplpoyerSignUp = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/employer-landing");
const ProductAndServices = () =>
  import(/* webpackChunkName: "group-landing" */ "@/components/landing-pages/product-services");
const PublicSearch = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/public-search");
const PublicResumeSearch = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/employer-resume");
const TopJobViewPublic = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/top-job-view");
const Pricing = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/pricing");
const PrivacyPolicy = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/privacy-policy");
const FAQ = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/faq");
const PublicCompany = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/company-details-page");
const PublicNews = () =>
  import(/*webpackChunkName: "group-landing" */ "@/components/landing-pages/news-public-view");
/* Candidate Pages */
const CandidateHome = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/candidate-home.vue");
const Myjob = () => import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/jobs/myjobs.vue");

const CandidateDashboard = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/candidate-dashboard.vue");
const FollowingCompanies = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/following/companies.vue");
const CandidateMessages = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/messages/messages.vue");
const CandidateResume = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/resume/resume.vue");
const CandidateProfile = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/profile.vue");
const CandidateSettings = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/settings/settings.vue");
const CandidateSearchJobs = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/jobs/search/search-job.vue");
const CandidateAppliedJobs = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/jobs/applied/applied-jobs.vue");
const CandidateJobsFromSameCompany = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/jobs/jobs-from-same-company/jobs-from-same-company.vue");
const CandidateSavedJobs = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/jobs/saved/saved-jobs.vue");
const TopJobView = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/top-job-view.vue");
const Allarticles = () =>
  import(/* webpackChunkName: "group-candidate" */ "@/components/candidate/news/allarticles.vue");

/*   Employer Pages  */

const EmployerHome = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/employer-home.vue");
const EmployerDashboard = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/dashboard.vue");
const Candidates = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/candidates/search-candidates");
const EmployerSettings = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/settings/settings.vue");
const EmployerMessages = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/messages/messages.vue");
const ManageAdmins = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/settings/manage-admins.vue");
const ManagePlan = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/plan/plan.vue");
const ManageProfile = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/profile/profile.vue");
const LegalDocuments = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/legal-documents/documents.vue");
const PostAJob = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/post-a-job/post-a-job-main.vue");
const ActiveJobs = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/active/active-jobs");
const ApprovedJobs = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/approved/approved-jobs");
const PendingJobs = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/pending/pending-jobs");
const ExpiredJobs = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/expired/expired-jobs");
const DraftJobs = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/draft/draft-jobs");
const JobDetails = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/jobs/phone-job-details");
const EmployerChangePassword = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/candidate/settings/settings.vue");
const ResumePreview = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/candidates/resume-builder");
const Cvbank = () =>
  import(/* webpackChunkName: "group-employer" */ "@/components/employer/cv-bank/main-page");
/* Admin Pages*/

const AdminHome = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/home");
const AdminDashboard = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/dashboard");
const AdminPendingJobs = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/manage-jobs/pending-jobs");
const AdminManageUsers = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/manage-managers/manage-users");
const AdminManagePlans = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/manage-plans/manage-plans");
const AdminPostAJob = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/post-a-job/post-a-job");
const LandingMessage = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/message");
const ErrorLog = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/error-log");
const MobileLog = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/log/mobile-log");
const HandoverCompany = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/handover/company");
const CreateCompany = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/handover/create-company");
const CompanyAllJobs = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/all-company-jobs/main_page");
const CreateNews = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/News/create-news");
const ManageNews = () => import(/* webpackChunkName: "group-admin" */ "../components/admin/News/manage-news");
const ManageAdminsAdmin = () =>
  import(/* webpackChunkName: "group-admin" */ "../components/admin/manage-admins/admin-page");

Vue.use(store);
Vue.use(Router);

let router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: LandingPage,
      meta: {
        isLandingPage: true
      },
      children: [
        {
          path: "/",
          name: "landingPage",
          component: IndexMiddle
        },
        {
          path: "/about",
          name: "about",
          component: IndexAboutUs
        },
        {
          path: "/contact",
          name: "contact",
          component: IndexContactUs
        },
        {
          path: "/signin",
          name: "signin",
          component: IndexSignIn
        },
        {
          path: "/signup",
          name: "signup",
          component: IndexSignUp
        },
        {
          path: "/forgotpassword",
          name: "forgotpassword",
          component: ResetPassword
        },
        {
          path: "/employersignup",
          name: "employersignup",
          component: EmplpoyerSignUp
        },
        {
          path: "/services",
          name: "services",
          component: ProductAndServices
        },
        {
          path: "/search/:q",
          name: "publicSearch",
          component: PublicSearch
        },
        {
          path: "/resume",
          name: "publicResumeSearch",
          component: PublicResumeSearch
        },
        {
          path: "/pricing",
          name: "pricing",
          component: Pricing
        },
        {
          path: "/company",
          name: "publicCompany",
          component: PublicCompany
        },
        {
          path: "/faq",
          name: "faq",
          component: FAQ
        },
        {
          path: "/privacy-policy",
          name: "privacyPolicy",
          component: PrivacyPolicy
        }
      ]
    },
    {
      path: "/candidate",
      name: "candidate",
      redirect: "/candidate/dashboard",
      meta: {
        isCandidate: true
      },
      component: CandidateHome,
      children: [
        {
          path: "dashboard",
          name: "candidateDashboard",
          component: CandidateDashboard
        },
        {
          path: "messages/",
          name: "candidateMessages",
          component: CandidateMessages
        },
        {
          path: "myjobs",
          name: "Myjobs",
          component: Myjob,
          children: [
            {
              path: "saved",
              name: "candidateSavedJobs",
              component: CandidateSavedJobs
            },
            {
              path: "applied",
              name: "candidateAppliedJobs",
              component: CandidateAppliedJobs
            }
          ]
        },
        {
          path: "scjobs/:cid/:cn",
          name: "candidateJobsFromSameCompany",
          component: CandidateJobsFromSameCompany
        },
        {
          path: "search/:q",
          name: "candidateSearchJobs",
          component: CandidateSearchJobs
        },
        {
          path: "resume",
          name: "candidateResume",
          component: CandidateResume
        },
        {
          path: "profile",
          name: "candidateProfile",
          component: CandidateProfile
        },
        {
          path: "allarticles",
          name: "allarticles",
          component: Allarticles
        },
        {
          path: "settings",
          name: "candidateSettings",
          component: CandidateSettings
        },
        {
          path: "topjobview/:q",
          name: "topJobView",
          component: TopJobView
        },
        {
          path: "following",
          name: "followingCompanies",
          component: FollowingCompanies
        },
        {
          path: "*",
          component: PageNotFound
        }
      ]
    },
    {
      path: "/employer",
      name: "employer",
      redirect: "/employer/dashboard",
      meta: {
        isEmployer: true
      },
      component: EmployerHome,
      children: [
        {
          path: "dashboard",
          name: "employerDashboard",
          component: EmployerDashboard
        },
        {
          path: "active",
          name: "activeJobs",
          component: ActiveJobs
        },
        {
          path: "approved",
          name: "approvedJobs",
          component: ApprovedJobs
        },
        {
          path: "pending",
          name: "pendingJobs",
          component: PendingJobs
        },
        {
          path: "expired",
          name: "expiredJobs",
          component: ExpiredJobs
        },
        {
          path: "draft",
          name: "draftJobs",
          component: DraftJobs
        },
        {
          path: "candidates",
          name: "candidates",
          component: Candidates
        },
        {
          path: "settings",
          name: "employerSettings",
          component: EmployerSettings
        },
        {
          path: "manage-admins",
          name: "manageAdmins",
          component: ManageAdmins
        },
        {
          path: "manage-plan",
          name: "managePlan",
          component: ManagePlan
        },
        {
          path: "manage-profile",
          name: "manageProfile",
          component: ManageProfile
        },
        {
          path: "legal-documents/",
          name: "legalDocuments",
          component: LegalDocuments
        },
        {
          path: "post-a-job",
          name: "postAJob",
          component: PostAJob
        },
        {
          path: "messages/",
          name: "employerMessages",
          component: EmployerMessages
        },
        {
          path: "jobdetails/",
          name: "jobdetails",
          component: JobDetails
        },
        {
          path: "change-password",
          name: "employerChangePassword",
          component: EmployerChangePassword
        },
        {
          path: "cv-bank",
          name: "cvbank",
          component: Cvbank
        },
        {
          path: "*",
          component: PageNotFound
        }
      ]
    },
    {
      path: "/candidate-resume-preview",
      name: "candidateResumePreview",
      meta: {
        isEmployer: true
      },
      component: ResumePreview
    },
    {
      path: "/admin",
      name: "admin",
      meta: {
        isAdmin: true
      },
      redirect: "/admin/dashboard",
      component: AdminHome,
      children: [
        {
          path: "dashboard",
          name: "adminDashboard",
          component: AdminDashboard
        },
        {
          path: "pending-jobs",
          name: "adminPendingJobs",
          component: AdminPendingJobs
        },
        {
          path: "manage-users/",
          name: "adminManageUsers",
          component: AdminManageUsers
        },
        {
          path: "manage-plans/",
          name: "adminManagePlans",
          component: AdminManagePlans
        },
        {
          path: "post-a-job",
          name: "adminPostAJob",
          component: AdminPostAJob
        },
        {
          path: "handover",
          name: "handover",
          component: HandoverCompany
        },
        {
          path: "create-company",
          name: "create-company",
          component: CreateCompany
        },
        {
          path: "create-news",
          name: "create-news",
          component: CreateNews
        },
        {
          path: "manage-news",
          name: "manage-news",
          component: ManageNews
        },
        {
          path: "manage-admins",
          name: "manage-admins",
          component: ManageAdminsAdmin
        },
        {
          path: "message",
          name: "message",
          component: LandingMessage
        },
        {
          path: "error-log",
          name: "error-log",
          component: ErrorLog
        },
        {
          path: "mobile-log",
          name: "mobileLog",
          component: MobileLog
        },
        {
          path: "all-jobs",
          name: "all-jobs",
          component: CompanyAllJobs
        },
        {
          path: "*",
          component: PageNotFound
        }
      ]
    },
    {
      path: "/confirmation",
      name: "confirmation",
      component: Confirmation
    },
    {
      meta: {
        is_job: true
      },
      path: "/jobdetails/:q",
      name: "topJobViewPublic",
      component: TopJobViewPublic
    },
    {
      meta: {
        is_pub: true
      },
      path: "/news",
      name: "news",
      component: PublicNews
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.isLandingPage)) {
    if (store.getters.haveToken) {
      store.dispatch("tokenExpireCallBack").then(function (response) {
        if (response.status === 202) {
          if (response.data.user_type === 2) {
            let existing = localStorage.getItem("user");
            existing = existing ? JSON.parse(existing) : {};
            let currentEmailConfirmed = existing["email_confirmed"];
            let currentHasDoc = existing["has_legal_doc"];
            let currentRegStatus = existing["reg_status"];
            let currentOverrideStatus = existing["legal_doc_override"];

            if (
              response.data.email_confirmed === undefined ||
              response.data.email_confirmed === null
            ) {
              // existing["email_confirmed"] = currentEmailConfirmed;
            } else {
              existing["email_confirmed"] = response.data.email_confirmed;
            }
            if (
              response.data.has_legal_doc === undefined ||
              response.data.has_legal_doc === null
            ) {
              // existing["has_legal_doc"] = parseInt(currentHasDoc);
            } else {
              existing["has_legal_doc"] = parseInt(response.data.has_legal_doc);
            }
            if (
              response.data.reg_status === undefined ||
              response.data.reg_status === null
            ) {
              // existing["reg_status"] = parseInt(currentRegStatus);
            } else {
              existing["reg_status"] = parseInt(response.data.reg_status);
            }
            if (
              response.data.legal_doc_override === undefined ||
              response.data.legal_doc_override === null
            ) {
              // existing["legal_doc_override"] = parseInt(currentOverrideStatus);
            } else {
              existing["legal_doc_override"] = parseInt(response.data.legal_doc_override);
            }
            localStorage.setItem("user", JSON.stringify(existing));
            next({ path: "employer" });
          } else if (response.data.user_type === 3) {
            let existing = localStorage.getItem("user");
            existing = existing ? JSON.parse(existing) : {};
            let current = existing["email_confirmed"];
            if (
              response.data.email_confirmed === undefined ||
              response.data.email_confirmed === null
            ) {
              existing["email_confirmed"] = current;
            } else {
              existing["email_confirmed"] = response.data.email_confirmed;
            }
            localStorage.setItem("user", JSON.stringify(existing));
            next({ path: "candidate" });
          } else if (response.data.user_type === 1) {
            next({ path: "admin" });
          } else {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            next("/signin");
          }
        } else if (response.status === 406) {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/");
        }
      });
      return;
    }
    next();
  } else if (to.matched.some(record => record.meta.isCandidate)) {
    if (store.getters.haveToken) {
      store.dispatch("tokenExpireCallBack").then(function (response) {
        if (response.status === 202) {
          if (response.data.user_type === 3) {
            let existing = localStorage.getItem("user");
            existing = existing ? JSON.parse(existing) : {};
            let current = existing["email_confirmed"];
            if (
              response.data.email_confirmed === undefined ||
              response.data.email_confirmed === null
            ) {
              existing["email_confirmed"] = current;
            } else {
              existing["email_confirmed"] = response.data.email_confirmed;
            }
            localStorage.setItem("user", JSON.stringify(existing));
            next();
          } else {
            next({ path: "signup" });
          }
        } else if (response.status === 406) {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        }
      });
      return;
    }
    next("/signin");
  } else if (to.matched.some(record => record.meta.isEmployer)) {
    if (store.getters.haveToken) {
      store.dispatch("tokenExpireCallBack").then(function (response) {
        if (response.status === 202) {
          if (response.data.user_type === 2) {
            let existing = localStorage.getItem("user");
            existing = existing ? JSON.parse(existing) : {};
            let currentEmailConfirmed = existing["email_confirmed"];
            let currentHasDoc = existing["has_legal_doc"];
            let currentRegStatus = existing["reg_status"];
            let currentOverrideStatus = existing["legal_doc_override"]
            if (
              response.data.email_confirmed === undefined ||
              response.data.email_confirmed === null
            ) {
              // existing["email_confirmed"] = currentEmailConfirmed;
            } else {
              existing["email_confirmed"] = response.data.email_confirmed;
            }
            if (
              response.data.has_legal_doc === undefined ||
              response.data.has_legal_doc === null
            ) {
              // existing["has_legal_doc"] = parseInt(currentHasDoc);
            } else {
              existing["has_legal_doc"] = parseInt(response.data.has_legal_doc);
            }
            if (
              response.data.reg_status === undefined ||
              response.data.reg_status === null
            ) {
              // existing["reg_status"] = parseInt(currentRegStatus);
            } else {
              existing["reg_status"] = parseInt(response.data.reg_status);
            }
            if (
              response.data.legal_doc_override === undefined ||
              response.data.legal_doc_override === null || parseInt(response.data.legal_doc_override) === 0
            ) {
              // existing["legal_doc_override"] = parseInt(currentOverrideStatus);
            } else {
              existing["legal_doc_override"] = parseInt(response.data.legal_doc_override);
            }
            // console.log(existing)
            localStorage.setItem("user", JSON.stringify(existing));
            next();
          } else {
            next({ path: "signup" });
          }
        } else if (response.status === 406) {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        }
      });
      return;
    }
    next("/signin");
  } else if (to.matched.some(record => record.meta.isAdmin)) {
    if (store.getters.haveToken) {
      store.dispatch("tokenExpireCallBack").then(function (response) {
        if (response.status === 202) {
          if (response.data.user_type === 1) {
            next();
          } else {
            next({ path: "signup" });
          }
        } else if (response.status === 406) {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next("/signin");
        }
      });
      return;
    }
    next("/signin");
  } else if (to.matched.some(record => record.meta.is_job)) {
    if (store.getters.haveToken) {
      store.dispatch("tokenExpireCallBack").then(function (response) {
        if (response.status === 202) {
          if (response.data.user_type === 3) {
            next({
              name: 'topJobView',
              params: { q: to.params.q }
            });
          } else {
            next();
          }
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          next();
        }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
