import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Vuetify from "vuetify";
import axios from "axios";
import store from "./store/store";
import "vuetify/dist/vuetify.css";
import VueAnalytics from "vue-analytics";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "bootstrap/dist/css/bootstrap.css";
import Vuelidate from "vuelidate";
import "./css/global.css";
import * as CONSTANTS from "./globalConstants";
import VuePapaParse from "vue-papa-parse";
import $ from 'jquery';


// import GAuth from "vue-google-oauth2";
import firebase from "firebase/app"
import "firebase/database"
// const gauthOption = {
//   clientId:
//     "919550580533-vu6jhhlnbm06fds5gl0q4tlatba7knaa.apps.googleusercontent.com",
//   scope: "openid profile email",
//   prompt: "select_account"
// };
// Vue.use(GAuth, gauthOption);

Vue.use(VueAnalytics, {
  id: 'GTM-NPHV3LS',
  router
});

const firebaseConfig = {
  apiKey: "AIzaSyDvQbXUwQ-B3PRmnfeaKYFbx56B429wlbk",
  authDomain: "workd-ai.firebaseapp.com",
  databaseURL: "https://workd-ai.firebaseio.com",
  projectId: "workd-ai",
  storageBucket: "workd-ai.appspot.com",
  messagingSenderId: "159737598164",
  appId: "1:159737598164:web:40db93015456dffe542b41",
  measurementId: "G-TD5367S61Z"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

Vue.config.productionTip = false;
Vue.use(router);
Vue.use(store);
Vue.prototype.$http = axios;
Vue.prototype.$constants = CONSTANTS;
Vue.use(VuePapaParse);
Vue.use(Vuetify, {
  iconfont: "mdi" | "fa",
  theme: {
    primary: "#3b32ef",
    secondary: "#ff6e38",
    accent: "#00adee",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FFC107"
  }
});
Vue.use(Vuelidate);

new Vue({
  store: store,
  router: router,
  render: h => h(App)
}).$mount("#app");
