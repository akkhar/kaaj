import { MONTHS, PHONE_WIDTH, TAB_WIDTH } from "./globalConstants";
export const GET_ITEM_BY_KEY = function getItemByKey(items, key) {
  let i;
  for (i = 0; i < items.length; i++) {
    if (items[i].attrib_id === key.skill_id) {
      return true;
    }
  }
  return false;
};

export const GET_ITEM_BY_NAME = function getItemByKey(items, key) {
  let i;
  for (i = 0; i < items.length; i++) {
    if (items[i].skill_name.toLowerCase() === key.toLowerCase()) {
      return items[i];
    }
  }
  return null;
};

export const GET_ITEM_BY_KEY_LANGUAGE = function getItemByKey(items, key) {
  let i;
  for (i = 0; i < items.length; i++) {
    if (items[i].attrib_id === key.language_id) {
      return true;
    }
  }
  return false;
};

export const GET_ITEM_BY_NAME_LANGUAGE = function getItemByKey(items, key) {
  let i;
  for (i = 0; i < items.length; i++) {
    if (items[i].language_name.toLowerCase() === key.toLowerCase()) {
      return items[i];
    }
  }
  return null;
};

export const GET_STRING_FROM_ARRAY = function getStringFromArray(
  items,
  separator,
  exp_lvl
) {
  let result = "",
    i;
  for (i = 0; i < items.length; i++) {
    if (i === items.length - 1) {
      separator = ".";
    }
    let x = items[i].replace("time", "-time");
    if (exp_lvl) {
      result =
        result +
        x.charAt(0).toUpperCase() +
        x.slice(1) +
        SENIORITY_LEVEL_IN_YEARS(x) +
        separator;
    } else result = result + x.charAt(0).toUpperCase() + x.slice(1) + separator;
  }
  return result;
};

export const GET_STRING_FROM_LIST_OF_SKILLS = function getStringFromSkills(
  items,
  separator
) {
  let result = "",
    i;
  for (i = 0; i < items.length; i++) {
    if (i === items.length - 1) {
      separator = ".";
    }
    let x = items[i].skill_name;
    result = result + x.charAt(0).toUpperCase() + x.slice(1) + separator;
  }
  return result;
};
/**
 * @return {string}
 */
export const MONTH_DATE_FORMAT = function(date) {
  if (date.length > 10) date = GET_LOCAL_TIME_FROM_UTC(date);
  let value = date.substring(0, 10);
  let day = value.substring(0, 2);
  let month = value.substring(3, 5);
  let year = value.substring(6, 10);
  let utcDate1 = new Date(Date.UTC(year, month - 1, day));
  let time = utcDate1.toUTCString();
  return time.substring(0, time.length - 12);
};

export const SALARY_TEXT = function getSalaryText(min, max, negotiable, basis) {
  let x;
  x = min + "-" + max;
  if (basis && basis === "annually") {
    x = x + "/year";
  } else {
    x = x + "/month";
  }
  if (negotiable) {
    x = x + " (Negotiable)";
  } else {
    x = x + " (Non-negotiable)";
  }
  return x;
};

export const DateDiff = {
  inDays: function(d1, d2) {
    let t2 = d2.getTime();
    let t1 = d1.getTime();

    return parseInt((t2 - t1) / (24 * 3600 * 1000));
  },

  inWeeks: function(d1, d2) {
    let t2 = d2.getTime();
    let t1 = d1.getTime();

    return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
  },

  inMonths: function(d1, d2) {
    let d1Y = d1.getFullYear();
    let d2Y = d2.getFullYear();
    let d1M = d1.getMonth();
    let d2M = d2.getMonth();

    return d2M + 12 * d2Y - (d1M + 12 * d1Y);
  },

  inYears: function(d1, d2) {
    return d2.getFullYear() - d1.getFullYear();
  }
};

export const GET_YEARS = function() {
  const year = new Date().getFullYear();
  let years = [];
  let length = year - 1970;
  let i;
  for (i = 0; i <= length; i++) {
    years[i] = 1970 + i;
  }
  return years;
};

export const IsEmptyObject = function isEmpty(obj) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};
export const GET_TIME_FROM_TIMESTAMP = function getTimeFromTimeStamp(
  timestamp
) {
  let date = new Date(parseInt(timestamp) * 1000);
  let timeStamp = date.toString().substring(0, 25);
  return {
    time: timeStamp,
    day: date.getDay(),
    month: MONTHS[date.getMonth() - 1],
    year: date.getFullYear()
  };
};
export const PHONE = function() {
  if (window.innerWidth <= PHONE_WIDTH) return true;
  else return false;
};
export const TAB = function() {
  if (window.innerWidth <= TAB_WIDTH) return true;
  else return false;
};
export const IPHONE = function() {
  let device = navigator.platform.toLocaleLowerCase();
  if (device === "iphone" || device === "ipod" || device === "ipad")
    return true;
  else return false;
};

/**
 * @return {string}
 */
export const GET_INITIALS = function(name) {
  let text = name.split(" ");
  if (text.length > 1) {
    return text[0].charAt(0).toUpperCase() + text[1].charAt(0).toUpperCase();
  } else {
    return text[0].charAt(0).toUpperCase() + text[0].charAt(1).toUpperCase();
  }
};

/**
 * @return {string}
 */
export const GET_LOCAL_TIME_FROM_UTC = function(time) {
  let count = 0;
  let [day, month, year] = time.substr(0, 10).split("/");
  if (month.length === 1) {
    month = "0" + month;
    count++;
  }
  if (day.length === 1) {
    day = "0" + day;
    count++;
  }
  if (year.length > 4) year = year.substring(0, 4);
  let t1 = `${year}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
  let t2 = "T" + time.substr(11 - count, 8) + ".000Z";
  let localTime = new Date(t1 + t2);
  let f_year = localTime.getFullYear().toString()
  let f_month = (localTime.getMonth() + 1).toString()
  let f_date = localTime.getDate().toString()
  if(f_month.length === 1) {
    f_month = '0' + f_month
  }
  if(f_date.length === 1) {
    f_date = '0' + f_date
  }
  return `${f_date}/${f_month}/${f_year}`;
};

export const SENIORITY_LEVEL_IN_YEARS = function(level) {
  let levels = {
    junior: "(0-2 years)",
    associate: "(2-5 years)",
    senior: "(5-7 years)",
    staff: "(7+ years)"
  };
  return levels[level];
};
