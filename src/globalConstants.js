const base_api_url = process.env.VUE_APP_BASE_API_URL;
const sign_in_tail = "v1/sessions/";
const sign_in_succ = "v1/sessions/succeeding-signin";
const oauth_signup = "v1/users/candidate/oauth";
const confirm_email_tail = "v1/confirmation/";
const resend_email_tail = "v1/resend-email/";
const forgot_password_tail = "v1/set-password/";
const get_user_basic_info = "v1/users/";
const get_user_image = "v1/image";
const get_user_experience_info = "v1/candidate/experiences/";
const get_user_education_info = "v1/candidate/educations/";
const get_user_skills_info = "v1/candidate/skills/";
const validate_token = "v1/sessions/";
const auto_suggestion = "v1/auto-suggest/";
const upload_resume = "v1/resume";
const get_candidate_jobs_search = "v1/search/jobs/";
const candidate_action = "v1/candidate/actions/";
const save_candidate_filter = "v1/candidate/filters/";
const candidate_filter = "v1/employer/filters/";
const candidate_details = "v1/users/";
const candidate_invite = "v1/invite/";
const analytics = "v1/users/dash-board/";
const contact_us = "v1/users/contact-us/";
const employer_signup = "v1/users/employer/sign-up";
const filter_data = "v1/employer/filter/applicants/options";
const filter = "v1/employer/filter/applicants";
const follow_companies = "v1/candidate/follow";
const completion_status = "v1/candidate/profile/completion_status/";
const candidate_projects = "v1/candidate/projects/";
const candidate_languages = "v1/candidate/languages/";
const candidate_trainings = "v1/candidate/trainings/";
const candidate_digest = "v1/candidate/digest";
const candidate_filter_company = "v1/filter/companies";
const candidate_filter_jobs = "v1/filter";
const candidate_newsfeed = "v1/newsfeed";

/* Employer End*/
const employer_jobs = "v1/jobs";
const search_candidates = "v1/search/candidates/";
const job_applicants = "v1/employer/job/applicants";
const employer_action = "v1/employer/actions";
const messages = "v1/messages/";
const role_management = "v1/employer/";
const plan_management = "v1/employer/plan/";
const download_profile = "v1/download-profile";
const emp_upload_documents = "v1/employer/legal-doc";
const emp_cv_bank = "v1/employer/applicants";
const emp_cv_bank_filter = "v1/employer/filter/applicants/options";
const emp_cv_bank_filter_elem = "v1/employer/filter/applicants?";
const emp_cv_bank_search = "v1/employer/search/applicants";
const emp_job_import = "v1/job/import";

/*Admin End*/

const admin_jobs = "v1/admin/jobs/";
const admin_manage_users = "v1/admin/employers";
const admin_manage_featured_companies = "v1/admin/employers/featured";
const plans = "v1/admin/plans/";
const invoice = "v1/invoice";
const admin_auto_suggest_company_compliance =
  "v1/admin/auto-suggest/company/compliance/";
const admin_register_company = "v1/admin/employer/register";
const admin_register_admin = "v1/admin/utils/admin-actions";

/*public end*/

const public_job_details = "v1/jobs/public/";
const public_plans = "v1/public/plans/";
const public_search = public_job_details;
const public_resume_search = "v1/candidates/public/";
const public_company_profile = "v1/employer/company-details/public";
const news_management = "v1/news";
const super_news_management = "v1/news/all";

/* Common End*/
const job_details = "v1/job-details";
const job_same_company = "v1/jobs/company";
const public_news = "v1/news/public";


let candidate = {};
export const setCandidateInfo = function() {
  candidate = JSON.parse(localStorage.getItem("user")) || {};
};
export const getOwnerInfo = function() {
  return JSON.parse(localStorage.getItem("user")) || {};
};
export const getToken = function() {
  return candidate.token;
};
export const getUrl = function() {
  if (base_api_url.includes("dev")) return "https://dev.trimatra.me";
  else if (base_api_url.includes("stg")) return "https://stg.trimatra.me";
  else if (base_api_url.includes("workd")) return "https://workd.ai";
};

export const getBucketUrl = function() {
  if (base_api_url.includes("dev"))
    return "https://dev-trim-featured-company.s3-ap-southeast-1.amazonaws.com/featured_companies.json";
  else if (base_api_url.includes("stg"))
    return "https://stg-trim-featured-company.s3-ap-southeast-1.amazonaws.com/featured_companies.json";
  else if (base_api_url.includes("workd"))
    return "https://prd-trim-featured-company.s3-ap-southeast-1.amazonaws.com/featured_companies.json";
};

export const BASE_URL = {
  CONTACT_US: base_api_url + contact_us,
  EMPLOYER_SIGN_UP: base_api_url + employer_signup,
  SIGN_UP_API_URL: base_api_url + get_user_basic_info,
  SIGN_IN_API_URL: base_api_url + sign_in_tail,
  SIGN_IN_SUCC: base_api_url + sign_in_succ,
  OAUTH_SIGN_UP: base_api_url + oauth_signup,
  CONFIRM_EMAIL_API_URL: base_api_url + confirm_email_tail,
  RESEND_EMAIL_API_URL: base_api_url + resend_email_tail,
  FORGOT_PASSWORD_API_URL: base_api_url + forgot_password_tail,
  USER_DEMOGRAPHIC_INFO_URL: base_api_url + get_user_basic_info,
  USER_EXPERIENCES_INFO_URL: base_api_url + get_user_experience_info,
  USER_EDUCATIONAL_INFO_URL: base_api_url + get_user_education_info,
  USER_SKILLS_INFO_URL: base_api_url + get_user_skills_info,
  USER_IMAGE_ACTION: base_api_url + get_user_image,
  VALIDATE_TOKEN: base_api_url + validate_token,
  AUTO_SUGGESTION_GET_URL: base_api_url + auto_suggestion,
  EMPLOYER_JOBS: base_api_url + employer_jobs,
  RESUME: base_api_url + upload_resume,
  IMAGE: base_api_url + get_user_image,
  APPLICANT_FILTER: base_api_url + filter,
  ANALYTICS: base_api_url + analytics,
  CANDIDATE_JOB_SEARCH: base_api_url + get_candidate_jobs_search,
  CANDIDATE_ACTION: base_api_url + candidate_action,
  CANDIDATE_SIDE_FILTER: base_api_url + save_candidate_filter,
  INVITE_PEOPLE: base_api_url + candidate_invite,
  SEARCH_CANDIDATE: base_api_url + search_candidates,
  EMPLOYER_SIDE_FILTER: base_api_url + candidate_filter,
  JOB_APPLICANTS: base_api_url + job_applicants,
  EMPLOYER_ACTION: base_api_url + employer_action,
  ADMIN_JOBS: base_api_url + admin_jobs,
  CANDIDATE_DETAILS: base_api_url + candidate_details,
  PUBLIC_SEARCH: base_api_url + public_search,
  PUBLIC_JOB_DETAILS: base_api_url + public_job_details,
  MESSAGES: base_api_url + messages,
  EMPLOYER_ROLE: base_api_url + role_management,
  ADMIN_MANAGE_USERS: base_api_url + admin_manage_users,
  ADMIN_REGISTER_COMPANY: base_api_url + admin_register_company,
  GET_PLANS_ADMIN: base_api_url + plans,
  GET_FILTER_DATA: base_api_url + filter_data,
  SUBSCRIPTION_PLANS: base_api_url + public_plans,
  INVOICE_ACTION: base_api_url + invoice,
  EMPLOYER_PLAN_MANAGEMENT: base_api_url + plan_management,
  ADMIN_AUTO_SUGGEST_COMPANY_COMPLIANCE:
    base_api_url + admin_auto_suggest_company_compliance,
  FOLLOW_COMPANIES: base_api_url + follow_companies,
  PROFILE_COMPLETION_STATUS: base_api_url + completion_status,
  CANDIDATE_PROJECTS: base_api_url + candidate_projects,
  CANDIDATE_LANGUAGES: base_api_url + candidate_languages,
  PUBLIC_RESUME_SEARCH: base_api_url + public_resume_search,
  MAKE_FEATURED: base_api_url + admin_manage_featured_companies,
  GET_JOB_DETAILS: base_api_url + job_details,
  JOBS_FROM_SAME_COMPANY: base_api_url + job_same_company,
  DOWNLOAD_CANDIDATE_PROFILE: base_api_url + download_profile,
  PUBLIC_COMPANY_PROFILE: base_api_url + public_company_profile,
  CANDIDATE_TRAININGS: base_api_url + candidate_trainings,
  CANDIDATE_DIGEST: base_api_url + candidate_digest,
  EMPLOYER_UPLOAD_DOCUMENTS: base_api_url + emp_upload_documents,
  CANDIDATE_FILTER_COMPANIES: base_api_url + candidate_filter_company,
  CANDIDATE_FILTER_JOBS: base_api_url + candidate_filter_jobs,
  EMPLOYER_CV_BANK: base_api_url + emp_cv_bank,
  EMPLOYER_CV_BANK_FILTER: base_api_url + emp_cv_bank_filter,
  EMPLOYER_CV_BANK_FILTER_ELEM: base_api_url + emp_cv_bank_filter_elem,
  EMPLOYER_CV_BANK_SEARCH: base_api_url + emp_cv_bank_search,
  EMPLOYER_JOB_IMPORT: base_api_url + emp_job_import,
  ADMIN_NEWS_MANAGEMENT: base_api_url + news_management,
  CANDIDATE_NEWS_FEED: base_api_url + candidate_newsfeed,
  ADMIN_MANAGE_ADMIN: base_api_url + admin_register_admin,
  SUPER_ADMIN_MANAGE_ADMIN: base_api_url + super_news_management,
  NEWS_PUBLIC: base_api_url + public_news,
};

export const USER_TYPE = {
  ADMIN: 1,
  HIRING_MANAGER: 2,
  CANDIDATE: 3
};

export const HEADERS = {
  TYPE: {
    APPLICATION_JSON: "application/json",
    VERIFICATION: "verification",
    FORGOT_PASSWORD: "forgotPassword"
  }
};

export const METHOD = {
  GET: "get",
  POST: "post",
  DELETE: "delete",
  PUT: "put"
};

export const PAGE_NOT_READY_MESSAGE = "This page is not ready yet";
export const POPUP = {
  SUCCESS_MESSAGE: "Congratulations",
  FAILURE_MESSAGE: "Oops!"
};
export const REQUIRED_MESSAGE = {
  REQUIRED: "Required",
  NAME: "Full name must be between 4 to 64 characters long",
  PASSWORD: "Password is required",
  PASSWORD_LENGTH: "Password must be between 8 to 12 characters long",
  PASSWORD_NOT_MATCH: "Password do not match",
  PHONE_INVALID: "Must be a valid phone number",
  EMAIL_INVALID: "Must be a valid e-mail",
  EMAIL_OR_PHONE_INVALID: "Must be a valid e-mail or phone",
  LENGTH_TOO_LONG: "Too long",
  LENGTH_TOO_SHORT: "Too short",
  FIELD_LENGTH: "Length must be between 2 to 63 characters",
  END_YEAR: "End year can not be lesser than start year",
  INVALID: "Invalid",
  MAX_ITEM_3: "Max 3",
  MAX_ITEM_20: "Max 20",
  MAX_MONTHS: "Duration too long",
  END_MONTH:"End month can not be less than start month",
  VARIABLE_LENGHT:(start=3,end=63)=>`Length must be between ${start} to ${end} characters`
};
export const SIGN_UP = {
  SUCCESS:
    "Please click on the link that has just been sent to your email " +
    "account to verify your email and continue registration process.",
  FAILURE:
    "Sorry we can not create an account for you this time, please try again."
};
export const SIGN_IN = {
  SUCCESS: "Successfully logged in",
  FAILURE: "Sorry! "
};
export const EMAIL_VERIFICATION = {
  SUCCESS: "You have successfully verified your email address",
  FAILURE: "Sorry the link has expired. Resend a verification link."
};

export const RESEND_EMAIL = {
  SUCCESS:
    "A verification link has been sent successfully to your email account",
  FAILURE:
    "Sorry, we can't send a verification link right now. Please try again later or contact us."
};
export const RESET_PASSWORD = {
  SUCCESS: "You have successfully updated your password.",
  FAILURE:
    "Sorry, we could not update your password this time. This link might be expired. Please resend an verification email."
};
export const FORGET_PASSWORD = {
  TITLE: "Forgot Password?",
  MESSAGE: "Please enter your email address below"
};

export const USER_IMAGE = {
  SUCCESS: "IMAGE UPLOADED SUCCESSFULLY",
  ERROR_SIZE: "IMAGE SIZE IS TOO BIG",
  ERROR_TYPE: "YOUR IMAGE TYPE IS NOT SUPPORTED"
};
export const EDIT_PROFILE = {
  BAISC_INFO_SUCCESS: "INFO UPDATED SUCCESSFULLY",
  EXPERIENCE_UPDATE: "EXPERIENCE UPDATED SUCCESSFULLY",
  EDUCATION_UPDATE: "EDUCATION UPDATED SUCCESSFULLY",
  SKILL_UPDATE: "SKILL UPDATED SUCCESSFULLY"
};
export const USER_RESUME = {
  SUCCESS: "FILE UPLOADED SUCCESSFULLY",
  ERROR_SIZE: "FILE SIZE IS TOO BIG",
  ERROR_TYPE: "YOUR FILE TYPE IS NOT SUPPORTED"
};
export const CHANGE_PASSWORD = {
  SUCCESS: "PASSWORD CHANGED SUCCESSFULLY",
  ERROR_INCORRECT: "PASSWORD IS INCORRECT"
};
export const FILTER = {
  SAVE_FILTER: "FILTER SAVED",
  UPADTE_FILTER: "FILTER UPDATED"
};
export const APPLIED_JOB = "YOU HAVE SUCCESSFULLY APPLIED FOR THIS JOB";
export const SAVED_JOB = "YOU HAVE SUCCESSFULLY SAVED THIS JOB";
export const BUTTON = {
  OK: "OK",
  RESEND: "Send"
};
export const COMMON_ERROR = {
  ERROR: "NERWORK ERROR"
};
export const DROPDOWN_TEXT = {
  PROFILE: "Profile",
  SIGN_OUT: "Sign Out"
};

export const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export const JOB_CLOSING_MESSAGES = {
  CLOSE_JOB:
    "This action will permanently close this job. Are you Sure you want to close this job?",
  RE_OPEN_JOB:
    "This action will re-open this job in edit mode. You can edit and make necessary change and re-open the job. Are you Sure you want to re-open this job?",
  DISCARD_JOB:
    "This action will permanently discard this job. Are you Sure you want to discard this job?",
  APPROVE_JOB:
    "This action will approve this job. Are you Sure you want to approve this job?",
  EDIT_JOB: "Are you Sure you want to edit this job?"
};

export const QUERY_ACTION = {
  SKILL: "skill",
  COMPANY: "company",
  INSTITUTION: "institution",
  DEGREE: "degree",
  MAJOR: "major",
  INVOICE: "invoice",
  TITLE: "job_title",
  LANGUAGE: "language",
  QUERY_LENGTH: 100
};

export const ERROR_MESSAGES = {
  DEFAULT: "Something went wrong, try again later",
  SERVER_DOWN: "Something went wrong, try again later",
  UPDATED: "Updated Successfully",
  NOT_UPDATED: "Can not update your information right now. Try again later",
  CLOSE_THE_CURRENT_OPENED_BOX: "Please close the currently editing item"
};

export const RESPONSE_CODE = {
  SUCCESS: 200,
  CREATED: 201,
  PROCESSING: 202
};

export const RESUME_ACTION = {
  VIEW: 1,
  DOWNLOAD: 2
};

export const SNACKBAR_COLOR = {
  SUCCESS: "#43a046",
  SUCCESS_ALT: "#155724",
  ERROR: "#fb2832",
  ERROR_ALT: "#C82333",
  WARNING: "#ffb400",
  WARNING_ALT: "#f7e500"
};

export const SNACKBAR_ICON = {
  SUCCESS: "fa-check-circle",
  ERROR: "fa-exclamation-triangle"
};
export const PHONE_WIDTH = 600;
export const TAB_WIDTH = 800;

export const JOB_STATUS = {
  1: "init",
  2: "draft",
  3: "published",
  4: "approved",
  5: "closed",
  6: "discarded",
  7: "pending"
};

export const DELAY = {
  SEARCH_TIMEOUT: 1000, // 1 sec
  SEARCH_RESULT_MAX_DELAY: 20, // 20 call, 1 call/psec
  MESSAGE_COUNT_INTERVAL: 30000,
  MESSAGE_COUNT_INITIAL_INTERVAL: 10000,
  MAX_MESSAGE_COUNT_DELAY: 5,
  IMAGE_FETCH_DELAY: 300
};

export const ADMIN_ACTION = {
   10: "dashboard",
   20: "manage_job",
   30: "company",
   31: "new_company",
   32: "expired_company",
   33: "requested_company",
   34: "registered_company",
   35: "duplicate_company_name",
   36: "duplicate_company_email",
   40: "plan",
   41: "new_plan",
   42: "published_plan",
   43: "saved_plan",
   50: "post_job",
   51: "create_company",
   52: "create_user",
   53: "handover_company",
   54: "company_job",
   55: "message_candidate",
   56: "server_log",
   57: "error_log",
   58: "list_admin",
   59: "edit_admin"
};

export const ADMIN_ACTION_TEXT = {
    company: 30,
    company_job: 54,
    create_company: 51,
    create_user: 52,
    dashboard: 10,
    duplicate_company_email: 36,
    duplicate_company_name: 35,
    edit_admin: 59,
    error_log: 57,
    expired_company: 32,
    handover_company: 53,
    list_admin: 58,
    manage_job: 20,
    manage_news: 60,
    message_candidate: 55,
    new_company: 31,
    new_plan: 41,
    plan: 40,
    post_job: 50,
    published_plan: 42,
    registered_company: 34,
    requested_company: 33,
    saved_plan: 43,
    server_log: 56
};
