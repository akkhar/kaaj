module.exports = {
  pluginOptions: {
    s3Deploy: {
      registry: undefined,
      awsProfile: "default",
      region: process.env.VUE_APP_S3_REGION,
      bucket: process.env.VUE_APP_S3_BUCKET_NAME,
      createBucket: true,
      staticHosting: true,
      staticIndexPage: "index.html",
      staticErrorPage: "index.html",
      assetPath: "dist",
      assetMatch: "**",
      deployPath: "/",
      acl: "public-read",
      pwa: false,
      enableCloudfront: true,
      pluginVersion: "3.0.0",
      pwaFiles: "service-worker.js",
      cloudfrontId: process.env.VUE_APP_S3_CLOUDFRONT_ID,
      cloudfrontMatchers: "/*",
      uploadConcurrency: 5
    }
  }
};
