import { shallowMount,mount, config } from '@vue/test-utils'
import Education from '../../src/components/candidate/information/Education/education-card-edit.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('Education form validation', () => {
    //institution name
    it('Checks institution name REQUIRED', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ institution: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.instituteNameErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks institution happy path', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ institution: "University of dhaka" });
        wrapper.vm.onSave();
        expect(wrapper.vm.instituteNameErrors[0]).toBe(undefined)
    })
    it('Checks institution case:MAX LIMIT', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ institution: dummy.substring(0,64) });
        wrapper.vm.onSave();
        expect(wrapper.vm.instituteNameErrors[0]).toBe(REQUIRED_MESSAGE.FIELD_LENGTH)
    })
    it('Checks institution case:MIN LIMIT', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ institution:"1" });
        wrapper.vm.onSave();
        expect(wrapper.vm.instituteNameErrors[0]).toBe(REQUIRED_MESSAGE.FIELD_LENGTH)
    })
    //major name
    it('Checks major happy path', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ major: "University of dhaka" });
        wrapper.vm.onSave();
        expect(wrapper.vm.majorErrors[0]).toBe(undefined)
    })
    it('Checks major case:MAX LIMIT', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ major: dummy.substring(0,64) });
        wrapper.vm.onSave();
        expect(wrapper.vm.majorErrors[0]).toBe(REQUIRED_MESSAGE.FIELD_LENGTH)
    })
    it('Checks major case:MIN LIMIT', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ major:"1" });
        wrapper.vm.onSave();
        expect(wrapper.vm.majorErrors[0]).toBe(REQUIRED_MESSAGE.FIELD_LENGTH)
    })
    //degree
    it('Checks degree name REQUIRED', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ degree: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.degreeTypeErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //start date
    it('Checks start date REQUIRED', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ begin_date: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.beginDateErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //end date
    it('Checks end date REQUIRED', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ end_date: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //date issue
    it('Checks start date and end date month hiererchey', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ 
            begin_date: "1980-12",
            end_date: "1980-11" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.END_YEAR)
    })
    it('Checks start date and end date year hiererchey', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ 
            begin_date: "1980-12",
            end_date: "1970-12" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.END_YEAR)
    })
    it('Checks start year and end year hiererchey happy path', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ 
            begin_date: "1980-10",
            end_date: "1990-12" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(undefined)
    })
    // //description
    it('Checks description happy path', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ description: dummy.substring(0,2096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionErrors[0]).toBe(undefined)
    })
    it('Checks description case:MAX LIMIT', async () => {
        const wrapper = mount(Education);
        await wrapper.setData({ description: dummy.substring(0,4096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
})
