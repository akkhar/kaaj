import { mount, shallowMount,config } from '@vue/test-utils'
import SignUp from '../../src/components/landing-pages/sign-up-component'
import { REQUIRED_MESSAGE } from "../../src/globalConstants"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('SIGNUP FORM VALIDATION', () => {
    it('Checks name happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ firstName: "Example name" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(undefined)
    })
    it('Checks empty name', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ firstName: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks empty name case:MIN LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ firstName: "AA" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.NAME)
    })
    it('Checks empty name case:MAX LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ firstName: "TESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETEST" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.NAME)
    })
    it('Checks email happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@trimatra.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.emailErrors[0]).toBe(undefined)
    })
    it('Checks empty email', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "" });
        await wrapper.find("v-form").trigger("submit.prevent");
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks valid email CASE:test_user', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test_user@exmaple', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@exmaple" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test _ user@example.me', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test _ user@example.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE: test_user@example.me', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: " test_user@example.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test_user@example.me ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@example.me " });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })

    it('Checks password happy path',async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({password:"123456789012"});
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(undefined)
      })
    it('Checks empty password', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks password CASE:min limit', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "123456" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
    })
    it('Checks password case:max limit ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "1234567890123456" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
    })
    it('Checks repeat password happy path ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ 
            password: "123456789",
            repeatPassword:"123456789"
        });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.repeatPasswordErrors[0]).toBe(undefined)
    })
    it('Checks repeat password case: match ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ 
            password: "123456789",
            repeatPassword:"987654321"
        });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.repeatPasswordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_NOT_MATCH)
    })
})
