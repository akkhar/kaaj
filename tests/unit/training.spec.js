import { mount, config } from '@vue/test-utils'
import Training from '../../src/components/candidate/information/Training/training-card-edit.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('Training form validation', () => {
    //trainting tittle
    it('Checks trainting tittle REQUIRED', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_title: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.titleErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks trainting tittle happy path', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_title: "good work" });
        wrapper.vm.onSave();
        expect(wrapper.vm.titleErrors[0]).toBe(undefined)
    })
    it('Checks trainting tittle case:MAX LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_title: dummy.substring(0,128) });
        wrapper.vm.onSave();
        expect(wrapper.vm.titleErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(4,127))
    })
    it('Checks trainting tittle case:MIN LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_title:"123" });
        wrapper.vm.onSave();
        expect(wrapper.vm.titleErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(4,127))
    })
    //Institution name
    it('Checks Institution name REQUIRED', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ institution_name: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.institutionErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks Institution name happy path', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ institution_name: "good work" });
        wrapper.vm.onSave();
        expect(wrapper.vm.institutionErrors[0]).toBe(undefined)
    })
    it('Checks Institution name case:MAX LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ institution_name: dummy.substring(0,64) });
        wrapper.vm.onSave();
        expect(wrapper.vm.institutionErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(3,63))
    })
    it('Checks Institution name case:MIN LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ institution_name:"1" });
        wrapper.vm.onSave();
        expect(wrapper.vm.institutionErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(3,63))
    })
    //Training Duration
    it('Checks Training duration case:MAX LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_duration:101 });
        wrapper.vm.onSave();
        expect(wrapper.vm.durationErrors[0]).toBe(REQUIRED_MESSAGE.MAX_MONTHS)
    })
    it('Checks Training duration REQUIRED', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_duration:"" });
        wrapper.vm.onSave();
        expect(wrapper.vm.durationErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks Training duration happy path', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_duration:12 });
        wrapper.vm.onSave();
        expect(wrapper.vm.durationErrors[0]).toBe(undefined)
    })
    //Training year
    it('Checks Training year REQUIRED', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_year:"" });
        wrapper.vm.onSave();
        expect(wrapper.vm.trainingYearErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks Training year happy path', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_year:"2010" });
        wrapper.vm.onSave();
        expect(wrapper.vm.trainingYearErrors[0]).toBe(undefined)
    })
    //description
    it('Checks description happy path', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_description: dummy.substring(0,2096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionLengthErrors[0]).toBe(undefined)
    })
    it('Checks description case:MAX LIMIT', async () => {
        const wrapper = mount(Training);
        await wrapper.setData({ training_description: dummy.substring(0,4096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionLengthErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
})
