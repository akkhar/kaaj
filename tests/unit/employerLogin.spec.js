import { mount,config } from '@vue/test-utils'
import Login from '../../src/components/landing-pages/employer-sign-in'
import { REQUIRED_MESSAGE } from "../../src/globalConstants"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('LOGIN FORM VALIDATION', () => {
  it('Checks email happy path',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:"test_user@trimatra.me"});
    await wrapper.find("v-form").trigger("submit.prevent")
    // wrapper.vm.userSignIn();
    expect(wrapper.vm.emailErrors[0]).toBe(undefined)
  })
  it('Checks empty email',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:""});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
  })
  it('Checks valid email CASE:test_user',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:"test_user"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
  })
  it('Checks valid email CASE:test_user@exmaple',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:"test_user@exmaple"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
  })
  it('Checks valid email CASE:test _ user@example.me',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:"test _ user@example.me"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe("Must be a valid e-mail")
  })
  it('Checks valid email CASE: test_user@example.me',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:" test_user@example.me"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
  })
  it('Checks valid email CASE:test_user@example.me',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({email:"test_user@example.me "});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
  })
  it('Checks password happy path',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({password:"123456789012"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.passwordErrors[0]).toBe(undefined)
  })
  it('Checks empty password',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({password:""});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
  })
  it('Checks password CASE:min limit',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({password:"123456"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
  })
  it('Checks password case:max limit ',async () => {
    const wrapper = mount(Login);
    await wrapper.setData({password:"1234567890123456"});
    await wrapper.find("v-form").trigger("submit.prevent")
    expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
  })
})
