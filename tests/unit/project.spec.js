import { mount, config } from '@vue/test-utils'
import Project from '../../src/components/candidate/information/Projects/project-card-edit.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('Project form validation', () => {
    //institution name
    it('Checks project name REQUIRED', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_name: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.projectNameErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks project name happy path', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_name: "good work" });
        wrapper.vm.onSave();
        expect(wrapper.vm.projectNameErrors[0]).toBe(undefined)
    })
    it('Checks project name case:MAX LIMIT', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_name: dummy.substring(0,128) });
        wrapper.vm.onSave();
        expect(wrapper.vm.projectNameErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(4,127))
    })
    it('Checks project name case:MIN LIMIT', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_name:"123" });
        wrapper.vm.onSave();
        expect(wrapper.vm.projectNameErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(4,127))
    })
    //project url
    it('Checks project url happy path', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_url: dummy.substring(0,88) });
        wrapper.vm.onSave();
        expect(wrapper.vm.urlErrors[0]).toBe(undefined)
    })
    it('Checks project name case:MAX LIMIT', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_url: dummy.substring(0,101) });
        wrapper.vm.onSave();
        expect(wrapper.vm.urlErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
    //start date
    it('Checks start date REQUIRED', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ begin_date: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.beginDateErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //end date
    it('Checks end date REQUIRED', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ end_date: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //date issue
    it('Checks start date and end date month hiererchey', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ 
            begin_date: "1980-12",
            end_date: "1980-11" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.END_YEAR)
    })
    it('Checks start date and end date year hiererchey', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ 
            begin_date: "1980-12",
            end_date: "1970-12" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(REQUIRED_MESSAGE.END_YEAR)
    })
    it('Checks start year and end year hiererchey happy path', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ 
            begin_date: "1980-10",
            end_date: "1990-12" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endDateErrors[0]).toBe(undefined)
    })
    //description
    it('Checks description happy path', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_description: dummy.substring(0,2096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionErrors[0]).toBe(undefined)
    })
    it('Checks description case:MAX LIMIT', async () => {
        const wrapper = mount(Project);
        await wrapper.setData({ project_description: dummy.substring(0,4096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
})
