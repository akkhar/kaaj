import { shallowMount,mount, config } from '@vue/test-utils'
import BasicInfo from '../../src/components/candidate/information/Basic-Information/basic-info.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('Basic info form validation', () => {
    it('Checks phone happy path', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ phone: "01234567890" });
        wrapper.vm.savePhone();
        expect(wrapper.vm.phoneErrors[0]).toBe(undefined)
    })
    it('Checks phone case:12 digit', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ phone: "012345678912" });
        wrapper.vm.savePhone();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:10 digit', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ phone: "0123456789" });
        wrapper.vm.savePhone();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:ALPHA NUMERIC', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ phone: "0123df456789" });
        wrapper.vm.savePhone();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:ALPHABET', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ phone: "ABCDEFGHIJK" });
        wrapper.vm.savePhone();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    //address
    it('Checks location happy path', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ location: dummy.substring(0, 300) });
        wrapper.vm.saveLocation();
        expect(wrapper.vm.locationLengthErrors[0]).toBe(undefined)
    })
    it('Checks location case:MAX LIMIT', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ location: dummy.substring(0, 301) })
        wrapper.vm.saveLocation();
        expect(wrapper.vm.locationLengthErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
    //Linked in
    it('Checks linkedin happy path', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ linkedin: "https://www.linkedin.com/in/example" });
        wrapper.vm.saveLinkedin();
        expect(wrapper.vm.LinkedInErrors[0]).toBe(undefined)
    })
    it('Checks linkedin url validity', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ linkedin: "https://www.linkedin.com/example" });
        wrapper.vm.saveLinkedin();
        expect(wrapper.vm.LinkedInErrors[0]).toBe("Invalid URL")
    })
    it('Checks linkedin case:MAX LIMIT', async () => {
        const wrapper = mount(BasicInfo);
        await wrapper.setData({ linkedin: "https://www.linkedin.com/in/"+dummy.substring(0,73) });
        wrapper.vm.saveLinkedin()
        expect(wrapper.vm.LinkedInErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
})
