import { mount, config } from '@vue/test-utils'
import SignUp from '../../src/components/landing-pages/employer-sign-up.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('EMPLOYER SIGNUP FORM VALIDATION', () => {
    //name
    it('Checks name happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ name: "Example name" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(undefined)
    })
    it('Checks empty name', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ name: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks name case:MIN LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ name: "AA" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.NAME)
    })
    it('Checks name case:MAX LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ name: "TESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETESTNAMETEST" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.nameErrors[0]).toBe(REQUIRED_MESSAGE.NAME)
    })
    //company name
    it('Checks company name happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ companyName: "Example name" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.companyErrors[0]).toBe(undefined)
    })
    it('Checks empty company name', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ companyName: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.companyErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks company name case:MAX LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ companyName:dummy.substring(0,256) });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.companyErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
    //email
    it('Checks email happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@trimatra.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.emailErrors[0]).toBe(undefined)
    })
    it('Checks empty email', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "" });
        await wrapper.find("v-form").trigger("submit.prevent");
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks valid email CASE:test_user', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test_user@exmaple', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@exmaple" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test _ user@example.me', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test _ user@example.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE: test_user@example.me', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: " test_user@example.me" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    it('Checks valid email CASE:test_user@example.me ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ email: "test_user@example.me " });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.emailErrors[0]).toBe(REQUIRED_MESSAGE.EMAIL_INVALID)
    })
    //phone
    it('Checks phone happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone: "01234567891" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(undefined)
    })
    it('Checks empty phone', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks phone case:12 digit', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone:"012345678912" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:10 digit', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone:"0123456789" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:ALPHA NUMERIC', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone:"0123df456789" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    it('Checks phone case:ALPHABET', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ phone:"ABCDEFGHIJK" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.phoneErrors[0]).toBe(REQUIRED_MESSAGE.PHONE_INVALID)
    })
    //address
    it('Checks location happy path', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ location_name: "Example name" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.locationErrors[0]).toBe(undefined)
    })
    it('Checks empty location', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ location_name: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.locationErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks location case:MAX LIMIT', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ location_name:dummy.substring(0,301) });
        await wrapper.find("v-form").trigger("submit.prevent")
        // wrapper.vm.userSignIn();
        expect(wrapper.vm.locationErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
    //password
    it('Checks password happy path',async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({password:"123456789012"});
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(undefined)
      })
    it('Checks empty password', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks password CASE:min limit', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "123456" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
    })
    it('Checks password case:max limit ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ password: "1234567890123456" });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.passwordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_LENGTH)
    })
    //repeat password
    it('Checks repeat password happy path ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ 
            password: "123456789",
            repeatPassword:"123456789"
        });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.repeatPasswordErrors[0]).toBe(undefined)
    })
    it('Checks repeat password case: match ', async () => {
        const wrapper = mount(SignUp);
        await wrapper.setData({ 
            password: "123456789",
            repeatPassword:"987654321"
        });
        await wrapper.find("v-form").trigger("submit.prevent")
        expect(wrapper.vm.repeatPasswordErrors[0]).toBe(REQUIRED_MESSAGE.PASSWORD_NOT_MATCH)
    })
})
