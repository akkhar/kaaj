import { shallowMount,mount, config } from '@vue/test-utils'
import Experience from '../../src/components/candidate/information/Experience/experience-card-edit.vue'
import { REQUIRED_MESSAGE } from "../../src/globalConstants";
import { dummy } from "../../constant"
import Vue from 'vue'
config.silent = true;
Vue.config.silent = true;
describe('Experience form validation', () => {
    //company name
    it('Checks company name REQUIRED', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ company: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.companyErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks company name happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ company: "Trimatra Solutions" });
        wrapper.vm.onSave();
        expect(wrapper.vm.companyErrors[0]).toBe(undefined)
    })
    it('Checks company name case:MAX LIMIT', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ company: dummy.substring(0,256) });
        wrapper.vm.onSave();
        expect(wrapper.vm.companyErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(1,255))
    })
    // it('Checks company name case:MIN LIMIT', async () => {
    //     const wrapper = mount(Experience);
    //     await wrapper.setData({ company:"1" });
    //     wrapper.vm.onSave();
    //     expect(wrapper.vm.companyErrors[0]).toBe(REQUIRED_MESSAGE.FIELD_LENGTH)
    // })
    //position
    it('Checks position happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ position: "Software Engineer" });
        wrapper.vm.onSave();
        expect(wrapper.vm.positionErrors[0]).toBe(undefined)
    })
    it('Checks position REQUIRED', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ position: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.positionErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    it('Checks position case:MAX LIMIT', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ position: dummy.substring(0,128) });
        wrapper.vm.onSave();
        expect(wrapper.vm.positionErrors[0]).toBe(REQUIRED_MESSAGE.VARIABLE_LENGHT(1,127))
    })
    //start year
    it('Checks start year REQUIRED', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ start_year: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.startYearErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //end year
    it('Checks end yer REQUIRED', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ end_year: "" });
        wrapper.vm.onSave();
        expect(wrapper.vm.endYearErrors[0]).toBe(REQUIRED_MESSAGE.REQUIRED)
    })
    //date issue
    it('Checks start year and end year hiererchey', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ 
            start_year: "1980",
            end_year: "1970" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endYearErrors[0]).toBe(REQUIRED_MESSAGE.END_YEAR)
    })
    it('Checks start year and end year hiererchey happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ 
            start_year: "1980",
            end_year: "1990" 
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endYearErrors[0]).toBe(undefined)
    })
    it('Checks start month and end month hiererchey happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ 
            start_year: "1980",
            end_year: "1980",
            start_month: "May",
            end_month: "June"
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endMonthErrors[0]).toBe(undefined)
    })
    it('Checks start month and end month hiererchey', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ 
            start_year: "1980",
            end_year: "1980",
            start_month: "June",
            end_month: "May"
        });
        wrapper.vm.onSave();
        expect(wrapper.vm.endMonthErrors[0]).toBe(REQUIRED_MESSAGE.END_MONTH)
    })
    //description
    it('Checks description happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ work_description: dummy.substring(0,2096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionLengthErrors[0]).toBe(undefined)
    })
    it('Checks description case:MAX LIMIT', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ work_description: dummy.substring(0,4096) });
        wrapper.vm.onSave();
        expect(wrapper.vm.descriptionLengthErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
    //location
    it('Checks description happy path', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ location: dummy.substring(0,300) });
        wrapper.vm.onSave();
        expect(wrapper.vm.locationLengthErrors[0]).toBe(undefined)
    })
    it('Checks description case:MAX LIMIT', async () => {
        const wrapper = mount(Experience);
        await wrapper.setData({ location: dummy.substring(0,301) });
        wrapper.vm.onSave();
        expect(wrapper.vm.locationLengthErrors[0]).toBe(REQUIRED_MESSAGE.LENGTH_TOO_LONG)
    })
})
